[![pipeline status](https://gitlab.com/francois.travais/1tipi2bicycles/badges/master/pipeline.svg)](https://gitlab.com/francois.travais/1tipi2bicycles/commits/master)
[![coverage report](https://gitlab.com/francois.travais/1tipi2bicycles/badges/master/coverage.svg)](https://francois.travais.gitlab.io/1tipi2bicycles/)
[![Uptime Robot status](https://img.shields.io/uptimerobot/status/m778918918-3e92c097147760ee39d02d36.svg)](https://stats.uptimerobot.com/LvKLNUBwk)
[![Uptime Robot ratio (30 days)](https://img.shields.io/uptimerobot/ratio/m778918918-3e92c097147760ee39d02d36.svg)](https://stats.uptimerobot.com/LvKLNUBwk)

# 1 Tipi 2 Bicycles

Floriane & François travel blog

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

The following main components are required:
- `python 3.6`, other python 3 versions are not tested and the code is incompatible with python 2.7
- `pip`
- `npm >= 5+`, we are using `package-lock.json` introduced in NPM 5.0

Example on Fedora:
```
$ dnf install python3 python3-pip npm
```

And of course you need to clone this repository:

```
$ git clone https://gitlab.com/francois.travais/1tipi2bicycles.git
```

### Installing

1. Install frontend dependencies

```
npm install
```

2. Install python dependencies (preferably in a virtual environment)

```
# Create a new virtualenv
python3.6 virtualenv ~/.venvs/1tipi2bicycles-dev/
# Activate the virtualenv
source ~/.venvs/1tipi2bicycles-dev/bin/activate
# Install dev dependencies
pip install -r ottb/ottb/requirements/dev.txt
```

3. Compile translations

```
cd ottb/
python manage.py compilemessages
```

4. Prepare the database

```
# Add DB schema
python manage.py migrate
# Add translation fields
python manage.py sync_page_translation_fields --noinput
# Add the bare minimum data to the database
python manage.py loaddata site
```

5. Run the dev server

```
python ottb/manage.py runserver
```

6. Open the newly created website at `http://localhost:8000/` and start creating missing pages
in wagtail admin page: `http://localhost:8000/admin/`

## Running the tests

To run the tests you need to add test dependencies. If the dev dependencies are already installed you can move to step 2, otherwise run:

```
pip install -r ottb/ottb/requirements/test.txt
```

Then you have two options: run the tests with Redis and Postgres or with SQLite and a dummy cache.
Gitlab CI runs the tests with Redis and Postgres.

### Like in CI

Run the tests with Postgres and Redis, like in production:

1. Start a Redis and a Postgres on your machine directly or using docker

```
docker run -d --name ottb-postgres -p 5432:5432 postgres:9.3-alpine
docker run -d --name ottb-redis -p 6379:6379 redis:3.2-alpine
```

2. Run the tests

```
cd ottb
DJANGO_SETTINGS_MODULE=ottb.settings.test POSTGRES_HOST=localhost REDIS_HOST=localhost python run test
```

### The simplest way

Run the tests with SQLite and a dummy cache

```
cd ottb
DJANGO_SETTINGS_MODULE=ottb.settings.test DISABLE_DB_AND_CACHE=yes python run test
```

### Test the coding style

We use flake8 to make sure the coding style is respected.

1. Install code style dependencies

To run the code style check you first need to install code style tools. If the development dependencies are already installed move to step 2, otherwise run:

```
pip install -r ottb/ottb/requirements/codestyle.txt
```

2. Rune code style check

```
flake8
```

If nothing shows up and exit code is 0 then all is good.

## Python dependencies management

To manage dependencies we use [pip-tools](https://github.com/jazzband/pip-tools) to keep everything in order. Base dependencies are listed in `.in` files in `ottb/ottb/requirements/` and we use `pip-compile` to generate the matching `.txt` files.

Dependencies are split into several files according to the use:
- [base.in](ottb/ottb/requirements/base.in): common dependencies
- [production.in](ottb/ottb/requirements/production.in): dependencies for the production environment
- [test.in](ottb/ottb/requirements/test.in): dependencies required to test the project
- [codestyle.in](ottb/ottb/requirements/codestyle.in): dependencies required to check the codestyle
- [dev.in](ottb/ottb/requirements/dev.in): dependencies for the development

To generate `.txt` files, one simply run a script running itself `pip-compile` as follow:

```
./scripts/generate_requirements
```

The above script must be run every time a `.in` file is edited.

To make sure an existing virtual environment is in sync (add, update or remove a package) with one of the `.txt` file simply run:

```
# replace dev.txt by codestyle.txt, test.txt or production.txt dependening of your needs
pip-sync ottb/ottb/requirements/dev.txt
```


## Built With

* [Wagtail](https://wagtail.io/) - A great CMS built on top of Django
* [Django](https://www.djangoproject.com/) - The best python web framework ever
* [Docker](https://www.docker.com/) - The famous container platform

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

There is no versioning in place at the moment, only the commit hash.

## Authors

* **François Travais** - *Main work* - [francois.travais](https://gitlab.com/francois.travais) - [franvois-travais](https://github.com/francois-travais)
* **Charlie Hornsby** - *Initial help* - [chornsby](https://gitlab.com/chornsby) - [chornsby](https://github.com/chornsby)

See also the list of [contributors](https://gitlab.com/francois.travais/1tipi2bicycles/graphs/master) who participated in this project.

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to [PurpleBooth](https://github.com/PurpleBooth) for the [README](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2) and [CONTRIBUTING](https://gist.github.com/PurpleBooth/b24679402957c63ec426) templates
* Hat tip to [IQAndreas](https://github.com/IQAndreas) for the Markdown version of [Apache License 2.0](https://github.com/IQAndreas/markdown-licenses/blob/master/apache-v2.0.md)

