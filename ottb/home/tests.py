from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.test.testcases import TestCase
from wagtail.core.models import Page
from wagtail.images.models import Image
from wagtail.tests.utils import WagtailPageTests

from base.test_utils import get_richtext_json
from blog.models import BlogIndexPage
from home.models import HomePage
from roadbook.models import RoadbookIndexPage


class TestHomePageModel(TestCase):
    fixtures = ['site']

    def setUp(self):
        super(TestHomePageModel, self).setUp()
        self.root_page = Page.objects.get(id=1)

    def test_minimal_page(self):
        home_page = HomePage(
            title_en='Home',
            title_fr='Accueil',
            slug_en='home2',
            slug_fr='accueil2',
            banner_image=Image.objects.get(),
        )
        self.root_page.add_child(instance=home_page)
        # check that about_us_page exists in the db
        retrieved_page = HomePage.objects.get(id=home_page.id)
        # assert all fields
        self.assertEqual(retrieved_page.title_en, home_page.title_en)
        self.assertEqual(retrieved_page.title_fr, home_page.title_fr)
        self.assertEqual(retrieved_page.slug_en, home_page.slug_en)
        self.assertEqual(retrieved_page.slug_fr, home_page.slug_fr)
        self.assertEqual(retrieved_page.banner_image, home_page.banner_image)
        self.assertIsNone(retrieved_page.intro_en)
        self.assertIsNone(retrieved_page.intro_fr)
        self.assertIsNone(retrieved_page.body_en)
        self.assertIsNone(retrieved_page.body_fr)
        self.assertIsNone(retrieved_page.link_to_blog)
        self.assertIsNone(retrieved_page.link_to_blog_intro_en)
        self.assertIsNone(retrieved_page.link_to_blog_intro_fr)
        self.assertIsNone(retrieved_page.link_to_roadbook)
        self.assertIsNone(retrieved_page.link_to_roadbook_intro_en)
        self.assertIsNone(retrieved_page.link_to_roadbook_intro_fr)

    def test_banner_is_required(self):
        with self.assertRaises(ValidationError):
            self.root_page.add_child(instance=HomePage(
                title_en='Home',
                title_fr='Accueil',
                slug_en='home2',
                slug_fr='accueil2',
            ))

    def test_full_page(self):
        # Add a blog index page
        blog_index_page = BlogIndexPage(
            title_en='Blog',
            title_fr='Blog',
            slug_en='blog',
            slug_fr='blog',
        )
        HomePage.objects.get().add_child(instance=blog_index_page)
        # Add a road book index page
        roadbook_index_page = RoadbookIndexPage(
            title_en='Road book',
            title_fr='Carnet de route',
            slug_en='road-book',
            slug_fr='carnet-de-route',
        )
        HomePage.objects.get().add_child(instance=roadbook_index_page)
        home_page = HomePage(
            title_en='Home',
            title_fr='Accueil',
            slug_en='home2',
            slug_fr='accueil2',
            banner_image=Image.objects.get(),
            intro_en='A super intro',
            intro_fr='Une super intro',
            body_en='<p>This is the home page body</p>',
            body_fr="<p>C'est le corps de la page d'accueil</p>",
            link_to_blog=blog_index_page,
            link_to_blog_intro_en='<p>This is our blog</p>',
            link_to_blog_intro_fr='<p>Voici notre blog</p>',
            link_to_roadbook=roadbook_index_page,
            link_to_roadbook_intro_en='<p>This is our road book</p>',
            link_to_roadbook_intro_fr='<p>Voici notre carnet de route</p>',
        )
        self.root_page.add_child(instance=home_page)
        # check that about_us_page exists in the db
        retrieved_page = HomePage.objects.get(id=home_page.id)
        # assert all fields
        self.assertEqual(retrieved_page.title_en, home_page.title_en)
        self.assertEqual(retrieved_page.title_fr, home_page.title_fr)
        self.assertEqual(retrieved_page.slug_en, home_page.slug_en)
        self.assertEqual(retrieved_page.slug_fr, home_page.slug_fr)
        self.assertEqual(retrieved_page.banner_image, home_page.banner_image)
        self.assertEqual(retrieved_page.intro_en, home_page.intro_en)
        self.assertEqual(retrieved_page.intro_fr, home_page.intro_fr)
        self.assertEqual(retrieved_page.body_en, home_page.body_en)
        self.assertEqual(retrieved_page.body_fr, home_page.body_fr)
        self.assertEqual(retrieved_page.link_to_blog.id, blog_index_page.id)
        self.assertEqual(retrieved_page.link_to_blog_intro_en, home_page.link_to_blog_intro_en)
        self.assertEqual(retrieved_page.link_to_blog_intro_fr, home_page.link_to_blog_intro_fr)
        self.assertEqual(retrieved_page.link_to_roadbook.id, roadbook_index_page.id)
        self.assertEqual(retrieved_page.link_to_roadbook_intro_en, home_page.link_to_roadbook_intro_en)
        self.assertEqual(retrieved_page.link_to_roadbook_intro_fr, home_page.link_to_roadbook_intro_fr)


class FormAndViewTests(WagtailPageTests):
    fixtures = ['site']

    def setUp(self):
        super(FormAndViewTests, self).setUp()
        self.root_page = Page.objects.get(id=1)
        self.post_data = {
            'title_en': 'Home',
            'title_fr': 'Accueil',
            'slug_en': 'home2',
            'slug_fr': 'accueil2',
            'banner_image': Image.objects.get().pk,
            'intro_en': 'A super intro',
            'intro_fr': 'Une super intro',
            'body_en': get_richtext_json('This is the home page body'),
            'body_fr': get_richtext_json("C'est le corps de la page d'accueil"),
            'link_to_blog_intro_en': get_richtext_json('This is our blog'),
            'link_to_blog_intro_fr': get_richtext_json('Voici notre blog'),
            'link_to_roadbook_intro_en': get_richtext_json('This is our road book'),
            'link_to_roadbook_intro_fr': get_richtext_json('Voici notre carnet de route'),
        }

    def create_page(self):
        # Save new page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            # args: app_name, model_name, parent_page_id
            reverse('wagtailadmin_pages:add', args=('home', 'homepage', self.root_page.id)),
            self.post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        # Retrieve saved page
        return HomePage.objects.get(slug_en=self.post_data['slug_en'])

    def edit_default_home_page(self):
        response = self.client.post(
            reverse('wagtailadmin_pages:edit', args=(HomePage.objects.get().id,)),
            self.post_data
        )
        # Should redirect (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        return HomePage.objects.get(slug_en=self.post_data['slug_en'])


class TestHomePageForm(FormAndViewTests):

    def test_form_basic_populates_promote_tab_fields(self):
        # Save and retrieve the newly created page
        page = self.create_page()
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertEqual(page.search_description_en, 'A super intro')
        self.assertEqual(page.search_description_fr, 'Une super intro')

    def test_form_empty_populates_promote_tab_fields(self):
        del self.post_data['intro_en']
        del self.post_data['intro_fr']
        # Save and retrieve the newly created page
        page = self.create_page()
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertIsNone(page.search_description_en)
        self.assertIsNone(page.search_description_fr)

    def test_form_edit_populates_promote_tab_fields(self):
        # Save and retrieve the newly created page
        page = self.create_page()
        # Modify few fields and publish
        self.post_data['intro_en'] = 'Foo bar'
        self.post_data['title_fr'] = 'Plop'
        self.post_data['slug_fr'] = 'plop'
        self.post_data['action-publish'] = "Publish"
        # Save new page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            reverse('wagtailadmin_pages:edit', args=(page.id,)),
            self.post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        # Retrieve saved page
        page = HomePage.objects.get(slug_en='home2')
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertEqual(page.search_description_en, 'Foo bar')
        self.assertEqual(page.search_description_fr, 'Une super intro')


class TestHomePageView(FormAndViewTests):

    def setUp(self):
        super(TestHomePageView, self).setUp()
        # Publish after creation
        self.post_data['slug_en'] = 'home'
        self.post_data['slug_fr'] = 'accueil'
        self.post_data['action-publish'] = "Publish"
        self.blog_post_data = {
            'title_en': 'Blog',
            'title_fr': 'Blog',
            # Slugs are populated in javascript in wagtail admin
            'slug_en': 'blog',
            'slug_fr': 'blog',
            'intro_en': get_richtext_json('Hello world!'),
            'intro_fr': get_richtext_json('Salut le monde !'),
            'action-publish': 'Publish',
        }
        self.roadbook_post_data = {
            'title_en': 'Road book',
            'title_fr': 'Carnet de route',
            # Slugs are populated in javascript in wagtail admin
            'slug_en': 'road-book',
            'slug_fr': 'carnet-de-route',
            'total_length': '100',
            'action-publish': 'Publish'
        }

    def create_blog_and_roadbook_pages(self):
        # Save new page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            # args: app_name, model_name, parent_page_id
            reverse('wagtailadmin_pages:add', args=('blog', 'blogindexpage', HomePage.objects.get().id)),
            self.blog_post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        # Retrieve saved page
        blog_page = Page.objects.get(slug_en=self.blog_post_data['slug_en'])
        # Save new page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            # args: app_name, model_name, parent_page_id
            reverse('wagtailadmin_pages:add', args=('roadbook', 'roadbookindexpage', HomePage.objects.get().id)),
            self.roadbook_post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        # Retrieve saved page
        roadbook_page = Page.objects.get(slug_en=self.roadbook_post_data['slug_en'])
        return blog_page, roadbook_page

    def test_redirect_to_english_page(self):
        self.edit_default_home_page()
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.wsgi_request.LANGUAGE_CODE, 'en')
        self.assertEqual(response._headers['location'], ('Location', '/en/'))

    def test_redirect_to_french_page(self):
        self.edit_default_home_page()
        response = self.client.get('/', HTTP_ACCEPT_LANGUAGE='fr')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.wsgi_request.LANGUAGE_CODE, 'fr')
        self.assertEqual(response._headers['location'], ('Location', '/fr/'))

    def test_redirect_default_to_english_page(self):
        self.edit_default_home_page()
        response = self.client.get('/', HTTP_ACCEPT_LANGUAGE='pt')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.wsgi_request.LANGUAGE_CODE, 'en')
        self.assertEqual(response._headers['location'], ('Location', '/en/'))

    def test_view_without_links_get_english(self):
        self.edit_default_home_page()
        response = self.client.get('/en/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('home/home_page.html')
        self.assertInHTML('<h2 class="intro position-absolute p-3">A super intro</h2>', result, count=1)
        self.assertTagInHTML('<img alt="wordcloud3.png" width="2000" height="900" />', result, count=1,
                             allow_extra_attrs=True)
        self.assertInHTML('<p>This is the home page body</p>', result, count=1)
        self.assertInHTML('<a href="#" class="btn btn-secondary disabled" role="button">Coming soon...</a>', result,
                          count=2)
        self.assertInHTML('<p>This is our blog</p>', result, count=1)
        self.assertInHTML('<p>This is our road book</p>', result, count=1)

    def test_view_without_links_get_french(self):
        self.edit_default_home_page()
        response = self.client.get('/fr/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('home/home_page.html')
        self.assertInHTML('<h2 class="intro position-absolute p-3">Une super intro</h2>', result, count=1)
        self.assertTagInHTML('<img alt="wordcloud3.png" width="2000" height="900" />', result, count=1,
                             allow_extra_attrs=True)
        self.assertInHTML("<p>C&#x27;est le corps de la page d&#x27;accueil</p>", result, count=1)
        self.assertInHTML('<a href="#" class="btn btn-secondary disabled" role="button">Prochainement...</a>', result,
                          count=2)
        self.assertInHTML('<p>Voici notre blog</p>', result, count=1)
        self.assertInHTML('<p>Voici notre carnet de route</p>', result, count=1)

    def test_view_with_links_get_english(self):
        blog_page, roadbook_page = self.create_blog_and_roadbook_pages()
        self.post_data['link_to_blog'] = blog_page.id
        self.post_data['link_to_roadbook'] = roadbook_page.id
        self.edit_default_home_page()
        response = self.client.get('/en/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('home/home_page.html')
        self.assertInHTML('<h2 class="intro position-absolute p-3">A super intro</h2>', result, count=1)
        self.assertTagInHTML('<img alt="wordcloud3.png" width="2000" height="900" />', result, count=1,
                             allow_extra_attrs=True)
        self.assertInHTML('<p>This is the home page body</p>', result, count=1)
        self.assertInHTML('<a href="blog" class="btn btn-secondary" role="button">Blog</a>', result, count=1)
        self.assertInHTML('<p>This is our blog</p>', result, count=1)
        self.assertInHTML('<a href="road-book" class="btn btn-secondary" role="button">Road book</a>', result,
                          count=1)
        self.assertInHTML('<p>This is our road book</p>', result, count=1)

    def test_view_with_links_get_french(self):
        blog_page, roadbook_page = self.create_blog_and_roadbook_pages()
        self.post_data['link_to_blog'] = blog_page.id
        self.post_data['link_to_roadbook'] = roadbook_page.id
        self.edit_default_home_page()
        response = self.client.get('/fr/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('home/home_page.html')
        self.assertInHTML('<h2 class="intro position-absolute p-3">Une super intro</h2>', result, count=1)
        self.assertTagInHTML('<img alt="wordcloud3.png" width="2000" height="900" />', result, count=1,
                             allow_extra_attrs=True)
        self.assertInHTML("<p>C&#x27;est le corps de la page d&#x27;accueil</p>", result, count=1)
        self.assertInHTML('<p>Voici notre blog</p>', result, count=1)
        self.assertInHTML('<a href="blog" class="btn btn-secondary" role="button">Blog</a>', result, count=1)
        self.assertInHTML('<p>Voici notre carnet de route</p>', result, count=1)
        self.assertInHTML('<a href="carnet-de-route" class="btn btn-secondary" role="button">Carnet de route</a>',
                          result, count=1)
