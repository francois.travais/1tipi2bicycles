# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-23 14:27
from __future__ import unicode_literals

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('home', '0003_auto_20170918_2014'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='link_to_blog',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL,
                                    related_name='+', to='wagtailcore.Page'),
        ),
        migrations.AddField(
            model_name='homepage',
            name='link_to_roadbook',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL,
                                    related_name='+', to='wagtailcore.Page'),
        ),
    ]
