from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from home.models import HomePage


@register(HomePage)
class HomePageTR(TranslationOptions):
    fields = (
        'intro',
        'body',
        'link_to_blog_intro',
        'link_to_roadbook_intro'
    )
