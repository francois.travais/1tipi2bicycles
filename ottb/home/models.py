from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, PageChooserPanel
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel

from base.models import OttbPageWithIntroMixin


class HomePage(OttbPageWithIntroMixin, Page):
    intro = models.CharField(max_length=250, blank=True)
    banner_image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.SET_NULL, related_name='+', null=True
    )
    body = RichTextField(blank=True)
    link_to_blog = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )
    link_to_blog_intro = RichTextField(blank=True, verbose_name='Intro before link to blog')
    link_to_roadbook = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )
    link_to_roadbook_intro = RichTextField(blank=True, verbose_name='Intro before link to road book')

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('intro'),
            ImageChooserPanel('banner_image'),
        ], heading="Banner"),
        FieldPanel('body'),
        MultiFieldPanel([
            PageChooserPanel('link_to_blog', 'blog.BlogIndexPage'),
            FieldPanel('link_to_blog_intro'),
        ], heading="Blog link"),
        MultiFieldPanel([
            PageChooserPanel('link_to_roadbook', 'roadbook.RoadbookIndexPage'),
            FieldPanel('link_to_roadbook_intro'),
        ], heading="Roadbook ling"),
    ]
