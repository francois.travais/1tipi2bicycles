from django.test import SimpleTestCase
from django.utils.html import strip_tags

from base.utils import copy_localized_field


class TestLocalizedFieldsCopy(SimpleTestCase):

    def test_copy_to_empty_field(self):
        cleaned_data = {
            'something_else': 'whatever',
            'foo_en': 'toto',
            'foo_fr': 'titi',
            'bar_en': None,
            'bar_fr': None,
        }
        copy_localized_field(cleaned_data, 'foo', 'bar')
        self.assertEqual(cleaned_data, {
            'something_else': 'whatever',
            'foo_en': 'toto',
            'foo_fr': 'titi',
            'bar_en': 'toto',
            'bar_fr': 'titi',
        })

    def test_copy_to_existing_field(self):
        cleaned_data = {
            'something_else': 'whatever',
            'foo_en': 'toto',
            'foo_fr': 'titi',
            'bar_en': 'not good',
            'bar_fr': 'has to change',
        }
        copy_localized_field(cleaned_data, 'foo', 'bar')
        self.assertEqual(cleaned_data, {
            'something_else': 'whatever',
            'foo_en': 'toto',
            'foo_fr': 'titi',
            'bar_en': 'toto',
            'bar_fr': 'titi',
        })

    def test_copy_with_empty_source_value(self):
        cleaned_data = {
            'something_else': 'whatever',
            'foo_en': None,
            'foo_fr': 'titi',
            'bar_en': 'not good',
            'bar_fr': 'has to change',
        }
        copy_localized_field(cleaned_data, 'foo', 'bar')
        self.assertEqual(cleaned_data, {
            'something_else': 'whatever',
            'foo_en': None,
            'foo_fr': 'titi',
            'bar_en': None,
            'bar_fr': 'titi',
        })

    def test_copy_with_function(self):
        cleaned_data = {
            'something_else': 'whatever',
            'foo_en': 'toto',
            'foo_fr': 'titi',
            'bar_en': 'not good',
            'bar_fr': 'has to change',
        }

        def test_fn(value):
            return '-%s-' % value

        copy_localized_field(cleaned_data, 'foo', 'bar', test_fn)
        self.assertEqual(cleaned_data, {
            'something_else': 'whatever',
            'foo_en': 'toto',
            'foo_fr': 'titi',
            'bar_en': '-toto-',
            'bar_fr': '-titi-',
        })

    def test_copy_with_function_to_remove_html(self):
        cleaned_data = {
            'something_else': 'whatever',
            'foo_en': '<p>toto<hr\>Oh yeah!</p>',
            'foo_fr': 'titi',
            'bar_en': 'not good',
            'bar_fr': 'has to change',
        }

        copy_localized_field(cleaned_data, 'foo', 'bar', strip_tags)
        self.assertEqual(cleaned_data, {
            'something_else': 'whatever',
            'foo_en': '<p>toto<hr\>Oh yeah!</p>',
            'foo_fr': 'titi',
            'bar_en': 'totoOh yeah!',
            'bar_fr': 'titi',
        })
