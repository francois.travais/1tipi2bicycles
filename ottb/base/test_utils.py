from wagtail.admin.rich_text.converters.contentstate_models import Block, ContentState


def get_richtext_json(text, style='unstyled'):
    content_state = ContentState()
    block = Block(style)
    block.text = text
    content_state.blocks.append(block)
    return content_state.as_json()
