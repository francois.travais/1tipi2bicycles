from django import template

register = template.Library()


@register.inclusion_tag('tags/carousel.html')
def carousel(carousel_id, images):
    return {
        'carousel_id': carousel_id,
        'images': images,
    }
