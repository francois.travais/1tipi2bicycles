from modeltranslation import settings as mt_settings
from modeltranslation.utils import build_localized_fieldname


def copy_localized_field(d, src, dst, fn=None):
    """Copy one field to another for all available languages.
    Optionally transform the source field value with the provided function"""
    # Loop over languages configured for model translation
    for language in mt_settings.AVAILABLE_LANGUAGES:
        src_value = d[build_localized_fieldname(src, language)]
        # Apply the function on the source value is applicable
        if fn is not None:
            src_value = fn(src_value)
        d[build_localized_fieldname(dst, language)] = src_value


def get_field_in_all_languages(field):
    """Get field name in all available languages"""
    return [(language, build_localized_fieldname(field, language)) for language in mt_settings.AVAILABLE_LANGUAGES]
