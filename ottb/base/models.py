from django.db import models
from django.utils.html import strip_tags
from wagtail.admin.forms import WagtailAdminPageForm
from wagtailmetadata.models import MetadataMixin

from base.utils import copy_localized_field


class MetadataPageMixin(MetadataMixin, models.Model):
    """
    Custom version og wagtail metadata Page mixin
    """
    def get_meta_url(self):
        return self.full_url

    def get_meta_title(self):
        return self.seo_title or self.title

    def get_meta_description(self):
        return self.search_description

    def get_meta_image(self):
        try:
            return self.main_image()
        except AttributeError:
            return super(MetadataPageMixin, self).get_meta_image()

    class Meta:
        abstract = True


class OttbWagtailAdminPageForm(WagtailAdminPageForm):
    """
    WagtailAdminPageForm which automatically populates promotion titles for all languages
    """
    def clean(self):
        # Call super method clean
        cleaned_data = super(OttbWagtailAdminPageForm, self).clean()
        # Populate promotion title fields
        copy_localized_field(cleaned_data, 'title', 'seo_title')
        return cleaned_data


class OttbWagtailAdminPageWithIntroForm(OttbWagtailAdminPageForm):
    def clean(self):
        cleaned_data = super(OttbWagtailAdminPageWithIntroForm, self).clean()
        # Populate search descriptions in promotion tab
        copy_localized_field(cleaned_data, 'intro', 'search_description')
        return cleaned_data


class OttbWagtailAdminPageWithRichIntroForm(OttbWagtailAdminPageForm):
    def clean(self):
        cleaned_data = super(OttbWagtailAdminPageWithRichIntroForm, self).clean()
        # Populate search descriptions in promotion tab
        copy_localized_field(cleaned_data, 'intro', 'search_description', strip_tags)
        return cleaned_data


class OttbPageMixin(MetadataPageMixin, models.Model):
    base_form_class = OttbWagtailAdminPageForm

    class Meta:
        abstract = True


class OttbPageWithIntroMixin(MetadataPageMixin, models.Model):
    base_form_class = OttbWagtailAdminPageWithIntroForm

    class Meta:
        abstract = True


class OttbPageWithRichIntroMixin(MetadataPageMixin, models.Model):
    base_form_class = OttbWagtailAdminPageWithRichIntroForm

    class Meta:
        abstract = True
