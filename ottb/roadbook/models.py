import geojson
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import (
    CASCADE, PROTECT, BooleanField, Case, CharField, DateField, F, ForeignKey, OuterRef, PositiveIntegerField, Q,
    Subquery, When
)
from django.utils.functional import cached_property
from django.utils.html import strip_tags
from django.utils.translation import override as override_language, ugettext_lazy as _
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import FieldPanel, FieldRowPanel, InlinePanel, MultiFieldPanel
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core.fields import RichTextField
from wagtail.core.models import Orderable, Page
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.documents.models import Document
from wagtail.images.edit_handlers import ImageChooserPanel

from base.models import OttbPageMixin, OttbWagtailAdminPageForm
from base.utils import copy_localized_field, get_field_in_all_languages
from gps_track.models import GPSTrack, validate_gpx_file


def validate_gpx_document(document):
    """Raise ValidationError if the document cannot be parsed as a GPX file.

    Since the GPX is uploaded as a Document separately we know that it already
    exists. Now all we have to check is whether we can parse it.
    """
    validate_gpx_file(document.file)


class RoadbookPageMixin(object):
    def get_all_legs(self):
        """All published legs ordered by start date"""
        return RoadbookLegPage.objects.live().order_by('-start_date')


class RoadbookIndexPage(RoadbookPageMixin, OttbPageMixin, RoutablePageMixin, Page):
    """The main roadbook page linked in the top_menu.

    It shows a map and a list of all the child RoadbookPage entries which can
    be selected to zoom the map to that section.
    """
    # Roadbook index page is a direct child of the home page
    parent_page_types = ['home.HomePage']
    subpage_types = ['RoadbookLegPage', 'RoadbookRoutePage']

    total_length = PositiveIntegerField(default=10738)

    content_panels = Page.content_panels + [
        FieldPanel('total_length'),
    ]

    ajax_template = 'roadbook/includes/roadbook_item_list.html'

    def get_items(self):
        """Get direct descendant fegs and routes. Only routes with legs are retrieved.
        The order depends on the type of item:
        - if it's a leg: the start date
        - if it's a route: the latest start_date of the route's legs"""
        # Return legs of a specific route ordered by descending start dates
        # It's the subquery equivalent to the method descendant_of()
        get_legs_for_route = RoadbookLegPage.objects \
            .filter(Q(path__startswith=OuterRef('path')) & Q(depth__gte=OuterRef('depth'))) \
            .order_by('-start_date')
        # Get legs and routes, filtering out routes with no legs
        valid_legs_and_routes = self.get_children() \
            .live() \
            .filter(Q(roadbookroutepage__isnull=True) | Q(numchild__gt=0))
        # Extract the latest start_date of the route's legs as order_date,
        # if it's a leg just take start_date
        children_with_order_date = valid_legs_and_routes.annotate(
            order_date=Case(
                When(roadbooklegpage__isnull=True,
                     then=Subquery(get_legs_for_route.values('start_date')[:1])),
                default=F('roadbooklegpage__start_date')
            ))
        # Order by descending order_date
        return children_with_order_date.order_by('-order_date')

    def get_paginator(self):
        return Paginator(self.get_items(), 4)

    def get_context(self, request, page=1):
        # Pagination
        paginator = self.get_paginator()
        try:
            items = paginator.page(page)
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)

        # Update context
        context = super(RoadbookIndexPage, self).get_context(request)
        context['items'] = items
        return context

    @route(r'^(\d+)/$')
    def children_page(self, request, page):
        return self.serve(request, kwargs={'page': int(page)})

    @cached_property
    def uphill(self):
        return sum(child.specific.uphill for child in self.get_items())

    @cached_property
    def downhill(self):
        return sum(child.specific.downhill for child in self.get_items())


class RoadbookIndexChildPageMixin(RoadbookPageMixin):
    def next_page(self):
        return RoadbookLegPage.objects.live() \
                   .order_by('start_date') \
                   .filter(end_date__gt=self.end_date) \
                   .filter(start_date__gt=self.end_date)[0:1].get()

    def previous_page(self):
        return RoadbookLegPage.objects.live() \
                   .order_by('-end_date') \
                   .filter(start_date__lt=self.start_date) \
                   .filter(end_date__lt=self.start_date)[0:1].get()


class RoadbookLegPageForm(OttbWagtailAdminPageForm):
    def clean(self):
        cleaned_data = super(RoadbookLegPageForm, self).clean()
        # We validate the route_file here because on field level the validation is performed multiple times
        try:
            # route file presence is validated during field level validation
            if 'route_file' in cleaned_data:
                validate_gpx_document(cleaned_data['route_file'])
        except ValidationError as e:
            self.add_error('route_file', e.message)
        # Populate promotion tab based on description
        copy_localized_field(cleaned_data, 'description', 'search_description', strip_tags)
        return cleaned_data


class RoadbookLegPage(RoadbookIndexChildPageMixin, OttbPageMixin, Page):
    """An individual entry in the Roadbook with one route associated with it.

    This page has a single GPX file associated with it, some metadata about when
    the travelling happened, and some rich text content.
    """
    route_file = ForeignKey(
        'wagtaildocs.Document',
        on_delete=PROTECT,
        related_name='+',
    )
    start_date = DateField()
    end_date = DateField()
    description = RichTextField(blank=True)

    parent_page_types = ['RoadbookIndexPage', 'RoadbookRoutePage']
    subpage_types = []

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            DocumentChooserPanel('route_file'),
            FieldRowPanel([
                FieldPanel('start_date'),
                FieldPanel('end_date'),
            ]),
        ], _('Route')),
        FieldPanel('description'),
        InlinePanel('gallery_images', label="Gallery images"),
    ]

    base_form_class = RoadbookLegPageForm

    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        # Remove Geojson cache
        cache.delete(self.geojson_cache_key)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if 'update_fields' in kwargs and 'route_file' not in kwargs['update_fields']:
            # Most certainly a page revision save request
            return
        self.save_tracks()

    def save_tracks(self):
        # Delete cache if the model is updated
        cache.delete(self.geojson_cache_key)
        # Load or replace existing tracks based on route_file
        GPSTrack.load_gpx_document(Document.objects.get(id=self.route_file_id))

    def is_parent_a_route_page(self):
        return isinstance(self.get_parent().specific, RoadbookRoutePage)

    @property
    def tracks(self):
        return GPSTrack.objects.filter(gpx_document=self.route_file)

    def _sum_track_attribute(self, attribute):
        total = 0
        for track in self.tracks:
            d = getattr(track, attribute)
            if d:
                total += d
        return total

    @property
    def total_length(self):
        # m -> km
        return self._sum_track_attribute('length') / 1000

    @property
    def uphill(self):
        return self._sum_track_attribute('uphill')

    @property
    def downhill(self):
        return self._sum_track_attribute('downhill')

    @property
    def geojson_cache_key(self):
        """Cache key for the geojson content of this road book leg"""
        return '1tipi2bicycles.RoadbookLegPage.geojson.%s' % self.pk

    def _geojson(self):
        return geojson.dumps(geojson.FeatureCollection([t.geojson() for t in self.tracks]), allow_nan=True)

    def geojson(self):
        """Populate or fetch from the cache the geojson representation of this leg track.
        Caches forever since it won't change if the model isn't updated.
        """
        return cache.get_or_set(self.geojson_cache_key, self._geojson, None)


class RoadbookLegPageGalleryImage(Orderable):
    page = ParentalKey(RoadbookLegPage, related_name='gallery_images')
    image = ForeignKey(
        'wagtailimages.Image', on_delete=CASCADE, related_name='+'
    )
    caption = CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]


class RoadbookRoutePageForm(OttbWagtailAdminPageForm):
    def clean(self):
        cleaned_data = super(RoadbookRoutePageForm, self).clean()
        # Populate search description with start and end locations
        for (language, field) in get_field_in_all_languages('search_description'):
            # Activate the right language to get the text in the right language
            with override_language(language):
                cleaned_data[field] = '%s %s - %s' % (
                    _("Road book route:"), cleaned_data['start_location'], cleaned_data['end_location'])
        return cleaned_data


class RoadbookRoutePage(RoadbookIndexChildPageMixin, OttbPageMixin, Page):
    start_location = CharField(max_length=250)
    end_location = CharField(max_length=250)
    display_locations = BooleanField(default=False)

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('start_location'),
            FieldPanel('end_location'),
            FieldPanel('display_locations'),
        ], 'Locations'),
    ]

    parent_page_types = ['RoadbookIndexPage']
    subpage_types = ['RoadbookLegPage']

    base_form_class = RoadbookRoutePageForm

    def _get_legs(self):
        """All published children legs"""
        return RoadbookLegPage.objects.descendant_of(self).live()

    def get_legs_ascending(self):
        """All published children legs ordered by ascending start date"""
        return self._get_legs().order_by('start_date')

    def get_legs_descending(self):
        """All published children legs ordered by descending start date"""
        return self._get_legs().order_by('-start_date')

    def has_legs(self):
        return self._get_legs().exists()

    def _get_route_or_leg(self, leg):
        if leg is not None and leg.specific.is_parent_a_route_page():
            return leg.specific.get_parent().specific
        return leg

    def next_page(self):
        next_leg = super(RoadbookRoutePage, self).next_page()
        return self._get_route_or_leg(next_leg)

    def previous_page(self):
        previous_leg = super(RoadbookRoutePage, self).previous_page()
        return self._get_route_or_leg(previous_leg)

    @property
    def first_leg(self):
        if self.has_legs():
            return self.get_legs_ascending().first()
        return None

    @property
    def last_leg(self):
        if self.has_legs():
            return self.get_legs_descending().first()
        return None

    @property
    def start_date(self):
        if self.has_legs():
            return self.first_leg.start_date
        return None

    @property
    def end_date(self):
        if self.has_legs():
            return self.last_leg.end_date
        return None

    @cached_property
    def total_length(self):
        return sum(leg.total_length for leg in self._get_legs())

    @cached_property
    def uphill(self):
        return sum(leg.uphill for leg in self._get_legs())

    @cached_property
    def downhill(self):
        return sum(leg.downhill for leg in self._get_legs())
