from django.core.management.base import BaseCommand

from roadbook.models import RoadbookLegPage


class Command(BaseCommand):
    help = 'Updated all GPS tracks using each leg GPX documents'
    requires_migrations_checks = True
    output_transaction = True

    def add_arguments(self, parser):
        parser.add_argument('--all', action='store_true', dest='all', default=False,
                            help='Update all tracks, not only tracks of live legs')

    def handle(self, *args, **options):
        # Loop over live pages to reload tracks
        if options['all']:
            legs = RoadbookLegPage.objects.all()
        else:
            legs = RoadbookLegPage.objects.live()
        count = 0
        for leg in legs:
            if options['verbosity'] == 3:
                self.stdout.write('Updating GPS track "%s" of leg "%s"...' % (leg.route_file, leg))
            leg.save_tracks()
            if options['verbosity'] == 2:
                self.stdout.write('GPS track "%s" of leg "%s" updated' % (leg.route_file, leg))
            count += 1
        self.stdout.write(self.style.SUCCESS('Successfully updated %s tracks' % count))
