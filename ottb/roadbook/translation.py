from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from roadbook.models import RoadbookIndexPage, RoadbookLegPage, RoadbookLegPageGalleryImage, RoadbookRoutePage


@register(RoadbookIndexPage)
class RoadbookIndexPageTR(TranslationOptions):
    pass


@register(RoadbookLegPage)
class RoadbookLegPageTR(TranslationOptions):
    fields = (
        'description',
    )


@register(RoadbookLegPageGalleryImage)
class RoadbookLegPageGalleryImageTR(TranslationOptions):
    fields = (
        'caption',
    )


@register(RoadbookRoutePage)
class RoadbookRoutePageTR(TranslationOptions):
    pass
