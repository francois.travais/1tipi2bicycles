var ottbRoadbook = {
    map: null,
    legs: {},
    pagination: {
        page: 1
    },
    styles: {
        normal: {
            startEndPoint: {
                radius: 3,
                fillColor: "#234C68",// dark-shade
                color: "#10222F", // darker-shade
                weight: 1,
                opacity: 1,
                fillOpacity: 1
            },
            trackLine: {
                color: "#234C68" // dark-shade
            }
        },
        focused: {
            startEndPoint: {
                radius: 4,
                fillColor: "#75A9CF",// brand
                color: "#75A9CF", // brand
                weight: 1,
                opacity: 1,
                fillOpacity: 1
            },
            trackLine: {
                color: "#75A9CF" // brand
            }
        }
    }
};

function drawMarker(coordinates, title, popup, icon) {
    var marker = L.marker(coordinates, {
        title: title,
        icon: L.icon.glyph({
            prefix: 'fa',
            glyph: icon
        })
    }).addTo(ottbRoadbook.map);
    marker.bindPopup(popup);
    marker.on('click', function (e) {
        map.setView(e.latlng, 7);
    });
}

function _drawLeg(legId, style) {
    if (undefined !== ottbRoadbook.legs[legId].layer) {
        // leg is already shown. Remove it just in case it doesn't have the right style
        ottbRoadbook.legs[legId].layer.remove();
        ottbRoadbook.legs[legId].layer = undefined;
    }
    var layer = L.geoJSON(ottbRoadbook.legs[legId].tracks, {
        style: style.trackLine,
        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, style.startEndPoint);
        }
    })
    .bindPopup(ottbRoadbook.legs[legId].popup)
    .addTo(ottbRoadbook.map);
    // save it for later
    ottbRoadbook.legs[legId].layer = layer;
    return layer;
}

function drawLeg(legId) {
    return _drawLeg(legId, ottbRoadbook.styles.normal);
}

function drawLegFocused(legId) {
    return _drawLeg(legId, ottbRoadbook.styles.focused);
}

function zoomOnLegTrack(legId) {
    ottbRoadbook.map.flyToBounds(ottbRoadbook.legs[legId].layer.getBounds(), {
        maxZoom: 10,
    });
}

function cacheLegTrack(legId, popup, geojson) {
    if (undefined === ottbRoadbook.legs[legId]) {
        ottbRoadbook.legs[legId] = {};
    }
    ottbRoadbook.legs[legId].tracks = geojson;
    ottbRoadbook.legs[legId].popup = popup;
}

function requestLegTrack(legId, popup, geojsonUrl, onDone, onError) {
    var request = $.ajax({
        url: geojsonUrl,
        accepts: {
            geojson: 'application/geo+json'
        },
        converters: {
            'text geojson': jQuery.parseJSON
        },
        dataType: 'geojson'
    }).retry({times: 5, timeout: 1000});
    request.done(function (geojson) {
        cacheLegTrack(legId, popup, geojson);
        if (undefined !== onDone) {
            onDone(geojson);
        }
    });
    request.fail(function (jqXHR, textStatus) {
        console.error('Request for ' + legId + ' failed:', textStatus);
        if (undefined !== onError) {
            onError(textStatus);
        }
    });
}

function showLoadMoreButton() {
    $('#loading-more-legs').hide();
    $('#load-more-legs').show();
}

function showLoadingMoreButton() {
    $('#load-more-legs').hide();
    $('#loading-more-legs').show();
}

function hideLoadButtons() {
    $('#load-more-legs').hide();
    $('#loading-more-legs').hide();
}

function requestNextLegListPage() {
    if (ottbRoadbook.pagination.max_page !== ottbRoadbook.pagination.page) {
        showLoadingMoreButton();
        requestLegListPage(ottbRoadbook.pagination.page + 1, function () {
            ottbRoadbook.pagination.page += 1;
            if (ottbRoadbook.pagination.max_page === ottbRoadbook.pagination.page) {
                hideLoadButtons();
            } else {
                showLoadMoreButton();
            }
        }, function () {
            showLoadMoreButton();
        });
    } else {
        console.error('Cannot query for leg list page', ottbRoadbook.pagination.page, ', last page already shown')
    }
}

function requestLegListPage(page, onDone, onError) {
    var request = $.ajax(page + '/');
    request.done(function (leg_list) {
        $('.legs').append(leg_list);
        if (undefined !== onDone) {
            onDone(leg_list);
        }
    });
    request.fail(function (jqXHR, textStatus) {
        console.error('Request for road book legs page ' + page + ' failed:', textStatus);
        if (undefined !== onError) {
            onError(textStatus);
        }
    });
}
