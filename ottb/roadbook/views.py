from rest_framework import renderers
from rest_framework.response import Response
from rest_framework.views import APIView

from roadbook.models import RoadbookLegPage


class PlainTextGeoJsonRenderer(renderers.BaseRenderer):
    media_type = 'application/geo+json'
    format = 'geojson'

    def render(self, data, media_type=None, renderer_context=None):
        return data.encode(self.charset)


class RoadbookLegGeojsonView(APIView):
    renderer_classes = (PlainTextGeoJsonRenderer,)

    def get(self, request, pk):
        return Response(
            RoadbookLegPage.objects.get(pk=pk).specific.geojson(),
            headers={'Content-Disposition': 'attachment; filename=%s.geojson' % pk})
