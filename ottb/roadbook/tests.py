from wagtail.tests.utils import WagtailPageTests

from home.models import HomePage
from roadbook.models import RoadbookIndexPage, RoadbookLegPage, RoadbookRoutePage


class TestRoadbookIndexChildAndParentTypes(WagtailPageTests):
    def test_parent_page_type(self):
        self.assertAllowedParentPageTypes(RoadbookIndexPage, {HomePage})

    def test_child_page_type(self):
        self.assertAllowedSubpageTypes(RoadbookIndexPage, {RoadbookRoutePage, RoadbookLegPage})


class TestRoadbookRouteChildAndParentTypes(WagtailPageTests):
    def test_parent_page_type(self):
        self.assertAllowedParentPageTypes(RoadbookRoutePage, {RoadbookIndexPage})

    def test_child_page_type(self):
        self.assertAllowedSubpageTypes(RoadbookRoutePage, {RoadbookLegPage})


class TestRoadbookLegChildAndParentTypes(WagtailPageTests):
    def test_parent_page_type(self):
        self.assertAllowedParentPageTypes(RoadbookLegPage, {RoadbookIndexPage, RoadbookRoutePage})

    def test_child_page_type(self):
        self.assertAllowedSubpageTypes(RoadbookLegPage, [])
