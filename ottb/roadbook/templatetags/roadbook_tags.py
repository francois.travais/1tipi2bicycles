from django import template

register = template.Library()


@register.inclusion_tag('roadbook/tags/roadbook_map.html')
def roadbook_map(legs, leg=None):
    return {
        'legs': legs,
        'leg_page': leg
    }
