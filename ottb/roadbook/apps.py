from django.apps import AppConfig


class RoadbookConfig(AppConfig):
    name = 'roadbook'
