from __future__ import absolute_import, unicode_literals

from .base import *  # noqa: F403, F405

DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'xrjagdd_1)ofpe7^jc!^x4)&4@!^x29(958j biiig$)q93v9)7'


EMAIL_BACKEND = 'django.core.mail.backends.console.Dummy'

# Uncomment to enable compressor
COMPRESS_ENABLED = True

# Similar to production setup
COMPRESS_OFFLINE = True

STATIC_ROOT = os.getenv('DJANGO_STATIC_FILES_PATH')  # noqa: F405

if os.getenv('DISABLE_DB_AND_CACHE', 'no') != 'yes':  # noqa: F405
    # Database
    # https://docs.djangoproject.com/en/1.11/ref/settings/#databases
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'postgres',
            'USER': 'postgres',
            'HOST': os.getenv('POSTGRES_HOST', 'postgres'),  # noqa: F405
            'PORT': 5432,
        }
    }

    # Redis caching
    CACHES = {
        'default': {
            'BACKEND': 'django_redis.cache.RedisCache',
            'LOCATION': 'redis://%s:6379/1' % os.getenv('REDIS_HOST', 'redis'),  # noqa: F405
            'OPTIONS': {
                'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            }
        }
    }
    CACHE_MIDDLEWARE_ALIAS = 'default'
    CACHE_MIDDLEWARE_SECONDS = 900
else:
    # Dummy database
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),  # noqa: F405
        }
    }

    # Dummy caching
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache'
        }
    }

# Add translation fields to Wagtail Page
INSTALLED_APPS += [  # noqa: F405
    'wagtail_modeltranslation.migrate',
]

try:
    from .local import *  # noqa: F401, F403
except ImportError:
    pass
