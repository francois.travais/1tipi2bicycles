from __future__ import absolute_import, unicode_literals

from .base import *  # noqa: F403, F405

DEBUG = False

# Security
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# Front-end
COMPRESS_OFFLINE = True

STATIC_ROOT = os.getenv('DJANGO_STATIC_FILES_PATH')  # noqa: F405

SECRET_KEY = os.getenv('DJANGO_SECRET_KEY')  # noqa: F405

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': ('%(levelname)s %(asctime)s %(module)s %(process)d',
                       ' %(thread)d %(message)s')
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),  # noqa: F405
        },
    },
}

try:
    from .local import *  # noqa: F401, F403
except ImportError:
    pass
