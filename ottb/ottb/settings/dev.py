from __future__ import absolute_import, unicode_literals

from .base import *  # noqa: F403, F405

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'xrjagdd_1)puvq7^jc!^x4)&4@!^x29(9-+7)iiig$)q93v9)7'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Uncomment to enable compressor
# COMPRESS_ENABLED = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),  # noqa: F405
    }
}

# Dummy cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

try:
    from .local import *  # noqa: F401, F403
except ImportError:
    pass
