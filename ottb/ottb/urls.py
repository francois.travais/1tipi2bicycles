from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from feed import views as feed_views
from roadbook import views as roadbook_views

urlpatterns = [
    url(r'^django-admin/', include(admin.site.urls)),

    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),
    url(r'^roadbook-leg-track/(?P<pk>[0-9]+).geojson/$', roadbook_views.RoadbookLegGeojsonView.as_view()),
]

urlpatterns += i18n_patterns(
    # These URLs will have /<language_code>/ appended to the beginning
    url(r'^blog/', include('blog.urls', namespace="blog")),
    url(r'^feed/blog/$', feed_views.BlogLatestUpdates()),
    url(r'^feed/roadbook/$', feed_views.RoadbookLatestUpdates()),

    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    url(r'', include(wagtail_urls)),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.views.generic.base import TemplateView

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += url(r'^tests/404/$', TemplateView.as_view(template_name='404.html'), name='test404'),
    urlpatterns += url(r'^tests/500/$', TemplateView.as_view(template_name='500.html'), name='test500'),
