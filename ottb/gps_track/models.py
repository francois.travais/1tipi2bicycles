import geojson
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.utils import timezone
from gpxpy.gpx import GPXException, GPXXMLSyntaxException
from gpxpy.parser import GPXParser
from wagtail.core.models import Orderable
from wagtail.documents.models import Document


def parse_gpx_file(gpx_file):
    """Return a GPX instance parsed from the given file.

    This method assumes that everything will go fine and just lets exceptions
    bubble up if something goes wrong.
    """
    gpx_file.open()
    content = gpx_file.read().decode()  # Convert bytes -> str
    gpx_file.close()

    gpx_parser = GPXParser(content)
    return gpx_parser.parse()


def validate_gpx_file(gpx_file):
    """Raise ValidationError if the route file cannot be parsed as a GPX file.
    """
    try:
        parse_gpx_file(gpx_file)
    except GPXXMLSyntaxException:
        raise ValidationError('GPX file was not syntactically correct')
    except GPXException:
        raise ValidationError('GPX file contained invalid data')


class GPSTrack(Orderable):
    gpx_document = models.ForeignKey(
        Document,
        on_delete=models.PROTECT,
        related_name='+',
    )
    length = models.FloatField()
    uphill = models.FloatField()
    downhill = models.FloatField()
    elevation_min = models.FloatField()
    elevation_max = models.FloatField()
    moving_time = models.FloatField()
    stopped_time = models.FloatField()
    max_speed = models.FloatField()

    @classmethod
    def load_gpx_document(cls, gpx_document: Document):
        gpx = parse_gpx_file(gpx_document.file)
        to_create = []
        i = 0  # Fix the sort order to order of tracks in the XML

        for gpx_track in gpx.tracks:
            # Smooth elevations before anything else
            gpx_track.smooth()
            # Calculate track distance by taking elevation into account
            length = gpx_track.length_3d()
            uphill_downhill = gpx_track.get_uphill_downhill()
            min_max_elevation = gpx_track.get_elevation_extremes()
            moving_data = gpx_track.get_moving_data()
            # Simplify track
            gpx_track.simplify()
            gps_track = GPSTrack(
                gpx_document=gpx_document,
                length=length,
                uphill=uphill_downhill.uphill,
                downhill=uphill_downhill.downhill,
                elevation_min=min_max_elevation.minimum,
                elevation_max=min_max_elevation.maximum,
                moving_time=moving_data.moving_time,
                stopped_time=moving_data.stopped_time,
                max_speed=moving_data.max_speed,
                sort_order=i
            )
            to_create.append((gps_track, gpx_track.segments))
            i += 1

        with transaction.atomic():
            # Deletes also segments and points
            GPSTrack.objects.filter(gpx_document=gpx_document).delete()
            for track, gpx_segments in to_create:
                track.save()
                GPSTrackSegment.save_gpx_segments(gpx_segments, track)

    @property
    def start(self):
        return self.segments.first().points.first()

    @property
    def end(self):
        return self.segments.last().points.last()

    def geojson_start_end(self):
        return geojson.Feature(
            geometry=geojson.MultiPoint([self.start.position, self.end.position]),
            properties={'type': 'start_end_markers'})

    def geojson_lines(self):
        return [geojson.Feature(geometry=segment.geojson_linestring(), properties={'type': 'track_line'})
                for segment in self.segments.all()]

    def geojson(self):
        lines = self.geojson_lines()
        start_end_markers = self.geojson_start_end()
        lines.append(start_end_markers)
        return geojson.FeatureCollection(lines)


class GPSTrackSegment(Orderable):
    track = models.ForeignKey(GPSTrack, on_delete=models.CASCADE, related_name='segments')

    @classmethod
    def save_gpx_segments(cls, gpx_segments, track):
        created = []
        i = 0  # Fix the sort order to order of segments in the XML

        for gpx_segment in gpx_segments:
            gps_segment = GPSTrackSegment(track=track, sort_order=i)
            created.append(gps_segment.save())
            GPSTrackPoint.save_gpx_points(gpx_segment.points, gps_segment)
            i += 1

        return created

    def geojson_linestring(self):
        track_coordinates = []
        previous_coordinates = None
        for point in self.points.all():
            if previous_coordinates is not None:
                track_coordinates.append([previous_coordinates, point.position])
            previous_coordinates = point.position
        return geojson.MultiLineString(track_coordinates)


class GPSTrackPoint(Orderable):
    """A point on the route suitable to provide to some JS mapping software.

    These points are ephemeral and will be deleted and recreated whenever the
    related Page object is saved. They are effectively a more convenient cache
    of the point data from the GPX file. It is not meant to be edited directly.
    """
    segment = models.ForeignKey(GPSTrackSegment, on_delete=models.CASCADE, related_name='points')
    latitude = models.FloatField()
    longitude = models.FloatField()
    elevation = models.FloatField()
    time = models.DateTimeField(null=True)

    @classmethod
    def save_gpx_points(cls, gpx_points, segment):
        to_create = []
        i = 0  # Fix the sort order to order of points in the XML

        for point in gpx_points:
            gpx_point = GPSTrackPoint(
                segment=segment,
                latitude=point.latitude,
                longitude=point.longitude,
                elevation=point.elevation,
                time=timezone.make_aware(point.time, timezone.utc),
                sort_order=i,
            )
            to_create.append(gpx_point)
            i += 1

        return GPSTrackPoint.objects.bulk_create(to_create)

    @property
    def track(self):
        return self.segment.track

    @property
    def position(self):
        return self.longitude, self.latitude, self.elevation
