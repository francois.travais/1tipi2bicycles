from django.apps import AppConfig


class GpsTrackConfig(AppConfig):
    name = 'gps_track'
