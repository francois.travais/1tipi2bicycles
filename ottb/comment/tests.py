from django.template import Context, Template
from django.test import TestCase
from django.utils.translation import activate
from modeltranslation.utils import get_language
from wagtail.images.models import Image

from about_us.models import AboutUsPage
from home.models import HomePage


class TemplatetagTestCase(TestCase):
    fixtures = ['site']

    # Inspired from compressor test_templatetags.py
    def render(self):
        """
                A shortcut for testing template output.
                """
        c = Context(self.context)
        t = Template(self.template)
        return t.render(c).strip()

    @classmethod
    def setUpTestData(cls):
        home_page = HomePage.objects.get()
        about_us_page = AboutUsPage(
            title_en='About Us',
            title_fr='Qui nous sommes',
            slug_en='about-us',
            slug_fr='qui-nous-sommes',
            intro_en='<p>Hello world!</p>',
            intro_fr='<p>Salut le monde !</p>',
            floriane_picture=Image.objects.get(),
            floriane_description_en='<p>Hello my name is Floriane</p>',
            floriane_description_fr="<p>Salut je m'appelle Floriane</p>",
            francois_picture=Image.objects.get(),
            francois_description_en='<p>Hello my name is François</p>',
            francois_description_fr="<p>Salut je m'appelle François</p>",
        )
        home_page.add_child(instance=about_us_page)
        # check that about_us_page exists in the db
        cls.about_us_page = AboutUsPage.objects.get(id=about_us_page.id)
        cls.context = {
            'page': cls.about_us_page,
        }
        cls.template = """{% load comment_tags %}{% show_comments page %}"""
        cls.language = get_language()

    def tearDown(self):
        # Reativate default language
        activate(self.language)

    def test_english_page(self):
        result = self.render()
        self.assertIn("this.page.url = 'http://localhost:8000/en/about-us/';", result)
        self.assertIn("this.page.identifier = '%s';" % self.about_us_page.pk, result)

    def test_french_page(self):
        activate('fr')
        # Assert that the above command does activate French translations
        self.assertEqual('http://localhost:8000/fr/qui-nous-sommes/', self.about_us_page.full_url)
        result = self.render()
        # Disqus comment URL should be the english one
        self.assertIn("this.page.url = 'http://localhost:8000/en/about-us/';", result)
        self.assertIn("this.page.identifier = '%s';" % self.about_us_page.pk, result)
