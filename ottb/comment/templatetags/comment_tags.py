from django import template
from wagtail_modeltranslation.contextlib import use_language

register = template.Library()


@register.inclusion_tag('comment/tags/disqus.html')
def show_comments(page):
    with use_language('en'):
        en_url = page.full_url
    return {
        'en_url': en_url,
        'page': page,
    }
