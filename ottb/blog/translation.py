from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from blog.models import BlogIndexPage, BlogPage, BlogPageGalleryImage


@register(BlogIndexPage)
class BlogIndexPageTR(TranslationOptions):
    fields = (
        'intro',
    )


@register(BlogPage)
class BlogPageTR(TranslationOptions):
    fields = (
        'intro',
        'body',
    )


@register(BlogPageGalleryImage)
class BlogPageGalleryImageTR(TranslationOptions):
    fields = (
        'caption',
    )
