from django import template

register = template.Library()


@register.inclusion_tag('blog/tags/tag_list.html')
def tag_list(post):
    return {
        'tags': post.tags
    }
