from base.test_utils import get_richtext_json
from blog.tests.utils import BlogIndexFormAndViewTests


class TestBlogIndexPageView(BlogIndexFormAndViewTests):

    def setUp(self):
        super(TestBlogIndexPageView, self).setUp()
        self.index_post_data['action-publish'] = "Publish"

    def assert_minimal_en_page(self, result):
        # Title
        self.assertInHTML('<h1 class="display-3">Blog</h1>', result, count=1)
        # Intro
        self.assertInHTML('<div class="lead"><div class="rich-text"><p>Hello world!</p></div></div>', result, count=1)

    def assert_minimal_fr_page(self, result):
        # Title
        self.assertInHTML('<h1 class="display-3">Blog</h1>', result, count=1)
        # Intro
        self.assertInHTML('<div class="lead"><div class="rich-text"><p>Salut le monde !</p></div></div>', result,
                          count=1)

    def assert_page_1_en(self, result):
        self.assertInHTML('<a href="/en/blog/blog-post-1/">Blog post 1</a>', result, count=1)
        self.assertInHTML('<h6 class="text-muted">Jan. 2, 2018</h6>', result, count=1)
        self.assertInHTML('<p class="lead">This is a wonderful intro</p>', result, count=1)

    def assert_page_1_fr(self, result):
        self.assertInHTML('<a href="/fr/blog/article-de-blog-1/">Article de blog 1</a>', result, count=1)
        self.assertInHTML('<h6 class="text-muted">2 Janvier 2018</h6>', result, count=1)
        self.assertInHTML('<p class="lead">C&#39;est une super intro</p>', result, count=1)

    def assert_page_2_en(self, result):
        self.assertInHTML('<a href="/en/blog/blog-post-2/">Blog post 2</a>', result, count=1)
        self.assertInHTML('<h6 class="text-muted">Jan. 1, 2018</h6>', result, count=1)
        self.assertInHTML('<p class="lead">Another intro</p>', result, count=1)
        self.assertTagInHTML('<img alt="wordcloud3.png">', result, count=1, allow_extra_attrs=True)

    def assert_page_2_fr(self, result):
        self.assertInHTML('<a href="/fr/blog/article-de-blog-2/">Article de blog 2</a>', result, count=1)
        self.assertInHTML('<h6 class="text-muted">1 Janvier 2018</h6>', result, count=1)
        self.assertInHTML('<p class="lead">Une autre intro</p>', result, count=1)
        self.assertTagInHTML('<img alt="wordcloud3.png">', result, count=1, allow_extra_attrs=True)

    def _prepare_page_2(self):
        self.post_data['title_en'] = 'Blog post 2'
        self.post_data['title_fr'] = 'Article de blog 2'
        self.post_data['slug_en'] = 'blog-post-2'
        self.post_data['slug_fr'] = 'article-de-blog-2'
        self.post_data['date'] = '2018-01-01'
        self.post_data['intro_en'] = 'Another intro'
        self.post_data['intro_fr'] = 'Une autre intro'
        self.post_data['body_en'] = get_richtext_json('Body of my second blog post')
        self.post_data['body_fr'] = get_richtext_json('Corps de mon second article de blog')
        # Add 2 tags
        self.post_data['tags'] = 'hello,world'
        # Add an image
        self.post_data['gallery_images-0-caption_en'] = 'a super caption'
        self.post_data['gallery_images-0-caption_fr'] = 'une super légende'
        self.post_data['gallery_images-0-DELETE'] = ''
        self.post_data['gallery_images-0-id'] = ''
        self.post_data['gallery_images-0-image'] = 1
        self.post_data['gallery_images-0-ORDER'] = 1
        self.post_data['gallery_images-TOTAL_FORMS'] = 1

    def test_get_empty_english_page(self):
        # Save and retrieve the newly created page
        self.create_index_page()
        response = self.client.get('/en/blog/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_en_page(result)
        # No blog post message
        self.assertInHTML('<p class="text-muted">No blog posts found</p>', result, count=1)

    def test_get_empty_french_page(self):
        # Save and retrieve the newly created page
        self.create_index_page()
        response = self.client.get('/fr/blog/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_fr_page(result)
        # No blog post message
        self.assertInHTML('<p class="text-muted">Aucun article de blog trouvé</p>', result, count=1)

    def test_get_english_page(self):
        # Save and retrieve the newly created page
        index_page = self.create_index_page()
        # Create blog pages
        # Blog page 1
        self.post_data['action-publish'] = "Publish"
        page1 = self.create_page(index_page=index_page)
        # Blog page 2
        self._prepare_page_2()
        page2 = self.create_page(index_page=index_page)
        response = self.client.get('/en/blog/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_en_page(result)
        posts = response.context_data['posts']
        self.assertEqual(2, posts.count())
        self.assertEqual(posts.first().pk, page2.pk)
        self.assertEqual(posts.last().pk, page1.pk)
        # Blog post 1
        self.assert_page_1_en(result)
        # Blog post 2
        self.assert_page_2_en(result)
        self.assertTagInHTML('<a href="/en/blog/tag/hello/" role="button">hello</a>', result, count=1,
                             allow_extra_attrs=True)
        self.assertTagInHTML('<a href="/en/blog/tag/world/" role="button">world</a>', result, count=1,
                             allow_extra_attrs=True)

    def test_get_french_page(self):
        # Save and retrieve the newly created page
        index_page = self.create_index_page()
        # Create blog pages
        # Blog page 1
        self.post_data['action-publish'] = "Publish"
        page1 = self.create_page(index_page=index_page)
        # Blog page 2
        self._prepare_page_2()
        page2 = self.create_page(index_page=index_page)
        response = self.client.get('/fr/blog/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_fr_page(result)
        posts = response.context_data['posts']
        self.assertEqual(2, posts.count())
        self.assertEqual(posts.first().pk, page2.pk)
        self.assertEqual(posts.last().pk, page1.pk)
        # Blog post 1
        self.assert_page_1_fr(result)
        # Blog post 2
        self.assert_page_2_fr(result)
        self.assertTagInHTML('<a href="/fr/blog/tag/hello/" role="button">hello</a>', result, count=1,
                             allow_extra_attrs=True)
        self.assertTagInHTML('<a href="/fr/blog/tag/world/" role="button">world</a>', result, count=1,
                             allow_extra_attrs=True)

    def test_get_tag_on_two_english_page(self):
        # Save and retrieve the newly created page
        index_page = self.create_index_page()
        # Create blog pages
        # Blog page 1
        # Add 2 tags
        self.post_data['tags'] = 'hello,foo'
        self.post_data['action-publish'] = "Publish"
        page1 = self.create_page(index_page=index_page)
        # Blog page 2
        self._prepare_page_2()
        page2 = self.create_page(index_page=index_page)
        response = self.client.get('/en/blog/tag/hello/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_en_page(result)
        posts = response.context_data['posts']
        self.assertEqual(2, posts.count())
        self.assertEqual(posts.first().pk, page2.pk)
        self.assertEqual(posts.last().pk, page1.pk)
        self.assertInHTML('<p>Viewing all blog posts tagged <strong>hello</strong></p>', result, count=1)
        # Blog post 1
        self.assert_page_1_en(result)
        self.assertTagInHTML('<a href="/en/blog/tag/hello/" role="button">hello</a>', result, count=2,
                             allow_extra_attrs=True)
        self.assertTagInHTML('<a href="/en/blog/tag/foo/" role="button">foo</a>', result, count=1,
                             allow_extra_attrs=True)
        # Blog post 2
        self.assert_page_2_en(result)
        self.assertTagInHTML('<a href="/en/blog/tag/world/" role="button">world</a>', result, count=1,
                             allow_extra_attrs=True)

    def test_get_tag_on_two_french_page(self):
        # Save and retrieve the newly created page
        index_page = self.create_index_page()
        # Create blog pages
        # Blog page 1
        # Add 2 tags
        self.post_data['tags'] = 'hello,monde'
        self.post_data['action-publish'] = "Publish"
        page1 = self.create_page(index_page=index_page)
        # Blog page 2
        self._prepare_page_2()
        page2 = self.create_page(index_page=index_page)
        response = self.client.get('/fr/blog/tag/hello/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_fr_page(result)
        posts = response.context_data['posts']
        self.assertEqual(2, posts.count())
        self.assertEqual(posts.first().pk, page2.pk)
        self.assertEqual(posts.last().pk, page1.pk)
        self.assertInHTML('<p>Articles de blog contenant le tag <strong>hello</strong></p>', result, count=1)
        # Blog post 1
        self.assert_page_1_fr(result)
        self.assertTagInHTML('<a href="/fr/blog/tag/hello/" role="button">hello</a>', result, count=2,
                             allow_extra_attrs=True)
        self.assertTagInHTML('<a href="/fr/blog/tag/monde/" role="button">monde</a>', result, count=1,
                             allow_extra_attrs=True)
        # Blog post 2
        self.assert_page_2_fr(result)
        self.assertTagInHTML('<a href="/fr/blog/tag/world/" role="button">world</a>', result, count=1,
                             allow_extra_attrs=True)

    def test_get_tag_on_one_english_page(self):
        # Save and retrieve the newly created page
        index_page = self.create_index_page()
        # Create blog pages
        # Blog page 1
        # Add 2 tags
        self.post_data['tags'] = 'hello,foo'
        self.post_data['action-publish'] = "Publish"
        self.create_page(index_page=index_page)
        # Blog page 2
        self._prepare_page_2()
        page2 = self.create_page(index_page=index_page)
        # Tag only on one blog page
        response = self.client.get('/en/blog/tag/world/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_en_page(result)
        posts = response.context_data['posts']
        self.assertEqual(1, posts.count())
        self.assertEqual(posts.first().pk, page2.pk)
        self.assertInHTML('<p>Viewing all blog posts tagged <strong>world</strong></p>', result, count=1)
        # Blog page 2
        self.assert_page_2_en(result)
        self.assertTagInHTML('<a href="/en/blog/tag/hello/" role="button">hello</a>', result, count=1,
                             allow_extra_attrs=True)
        self.assertTagInHTML('<a href="/en/blog/tag/world/" role="button">world</a>', result, count=1,
                             allow_extra_attrs=True)

    def test_get_tag_on_one_french_page(self):
        # Save and retrieve the newly created page
        index_page = self.create_index_page()
        # Create blog pages
        # Blog page 1
        # Add 2 tags
        self.post_data['tags'] = 'hello,monde'
        self.post_data['action-publish'] = "Publish"
        self.create_page(index_page=index_page)
        # Blog page 2
        self._prepare_page_2()
        page2 = self.create_page(index_page=index_page)
        response = self.client.get('/fr/blog/tag/world/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_fr_page(result)
        posts = response.context_data['posts']
        self.assertEqual(1, posts.count())
        self.assertEqual(posts.first().pk, page2.pk)
        self.assertInHTML('<p>Articles de blog contenant le tag <strong>world</strong></p>', result, count=1)
        # Blog post 2
        self.assert_page_2_fr(result)
        self.assertTagInHTML('<a href="/fr/blog/tag/hello/" role="button">hello</a>', result, count=1,
                             allow_extra_attrs=True)
        self.assertTagInHTML('<a href="/fr/blog/tag/world/" role="button">world</a>', result, count=1,
                             allow_extra_attrs=True)

    def test_get_unknown_tag_english_page(self):
        # Save and retrieve the newly created page
        index_page = self.create_index_page()
        # Create blog pages
        # Blog page 1
        # Add 2 tags
        self.post_data['tags'] = 'hello,foo'
        self.post_data['action-publish'] = "Publish"
        self.create_page(index_page=index_page)
        response = self.client.get('/en/blog/tag/bar/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_en_page(result)
        posts = response.context_data['posts']
        self.assertEqual(0, posts.count())
        self.assertInHTML('<p>Viewing all blog posts tagged <strong>bar</strong></p>', result, count=1)
        self.assertInHTML('<p class="text-muted">No blog posts found</p>', result, count=1)

    def test_get_unknown_tag_french_page(self):
        # Save and retrieve the newly created page
        index_page = self.create_index_page()
        # Create blog pages
        # Blog page 1
        # Add 2 tags
        self.post_data['tags'] = 'hello,foo'
        self.post_data['action-publish'] = "Publish"
        self.create_page(index_page=index_page)
        response = self.client.get('/fr/blog/tag/bar/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_index_page.html')
        self.assert_minimal_fr_page(result)
        posts = response.context_data['posts']
        self.assertEqual(0, posts.count())
        self.assertInHTML('<p>Articles de blog contenant le tag <strong>bar</strong></p>', result, count=1)
        self.assertInHTML('<p class="text-muted">Aucun article de blog trouvé</p>', result, count=1)


class TestBlogPageView(BlogIndexFormAndViewTests):

    def setUp(self):
        super(TestBlogPageView, self).setUp()
        self.index_page = self.create_index_page()
        self.post_data['action-publish'] = "Publish"

    def assert_basic_en_page(self, result):
        # Title
        self.assertInHTML('<h1>Blog post 1</h1>', result, count=1)
        # Date
        self.assertInHTML('<small>Jan. 2, 2018</small>', result, count=1)
        # Intro
        self.assertInHTML('<p class="lead">This is a wonderful intro</p>', result, count=1)
        # Body
        self.assertInHTML('<p>Body of my first blog post</p>', result, count=1)
        # Return to blog button
        self.assertInHTML('<a href="/en/blog/" class="btn btn-primary" role="button">Return to blog</a>', result,
                          count=1)

    def assert_basic_fr_page(self, result):
        # Title
        self.assertInHTML('<h1>Article de blog 1</h1>', result, count=1)
        # Date
        self.assertInHTML('<small>2 Janvier 2018</small>', result, count=1)
        # Intro
        self.assertInHTML('<p class="lead">C&#39;est une super intro</p>', result, count=1)
        # Body
        self.assertInHTML('<p>Corps de mon premier article de blog</p>', result, count=1)
        # Return to blog button
        self.assertInHTML('<a href="/fr/blog/" class="btn btn-primary" role="button">Retour au blog</a>', result,
                          count=1)

    def test_basic_get_english_page(self):
        # Save and retrieve the newly created page
        self.create_page()
        response = self.client.get('/en/blog/blog-post-1/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_page.html')
        self.assert_basic_en_page(result)

    def test_basic_get_french_page(self):
        # Save and retrieve the newly created page
        self.create_page()
        response = self.client.get('/fr/blog/article-de-blog-1/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_page.html')
        self.assert_basic_fr_page(result)

    def test_get_english_page(self):
        # Add 2 tags
        self.post_data['tags'] = 'hello,world'
        # Add an image
        self.post_data['gallery_images-0-caption_en'] = 'a super caption'
        self.post_data['gallery_images-0-caption_fr'] = 'une super légende'
        self.post_data['gallery_images-0-DELETE'] = ''
        self.post_data['gallery_images-0-id'] = ''
        self.post_data['gallery_images-0-image'] = 1
        self.post_data['gallery_images-0-ORDER'] = 1
        self.post_data['gallery_images-TOTAL_FORMS'] = 1
        # Save and retrieve the newly created page
        self.create_page()
        response = self.client.get('/en/blog/blog-post-1/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_page.html')
        self.assert_basic_en_page(result)
        # Tags
        self.assertTagInHTML('<a href="/en/blog/tag/hello/" role="button">hello</a>', result, count=1,
                             allow_extra_attrs=True)
        self.assertTagInHTML('<a href="/en/blog/tag/world/" role="button">world</a>', result, count=1,
                             allow_extra_attrs=True)
        # Carousel
        self.assertTagInHTML('<img alt="wordcloud3.png" class="carousel-image-landscape" width="1200" height="675">',
                             result, count=1, allow_extra_attrs=True)
        self.assertInHTML('<p>a super caption</p>', result, count=2)

    def test_get_french_page(self):
        # Add 2 tags
        self.post_data['tags'] = 'salut,monde'
        # Add an image
        self.post_data['gallery_images-0-caption_en'] = 'a super caption'
        self.post_data['gallery_images-0-caption_fr'] = 'une super légende'
        self.post_data['gallery_images-0-DELETE'] = ''
        self.post_data['gallery_images-0-id'] = ''
        self.post_data['gallery_images-0-image'] = 1
        self.post_data['gallery_images-0-ORDER'] = 1
        self.post_data['gallery_images-TOTAL_FORMS'] = 1
        # Save and retrieve the newly created page
        self.create_page()
        response = self.client.get('/fr/blog/article-de-blog-1/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('blog/blog_page.html')
        self.assert_basic_fr_page(result)
        # Tags
        self.assertTagInHTML('<a href="/fr/blog/tag/salut/" role="button">salut</a>', result, count=1,
                             allow_extra_attrs=True)
        self.assertTagInHTML('<a href="/fr/blog/tag/monde/" role="button">monde</a>', result, count=1,
                             allow_extra_attrs=True)
        # Carousel
        self.assertTagInHTML('<img alt="wordcloud3.png" class="carousel-image-landscape" width="1200" height="675">',
                             result, count=1, allow_extra_attrs=True)
        self.assertInHTML('<p>une super légende</p>', result, count=2)
