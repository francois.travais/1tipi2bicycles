from django.core.urlresolvers import reverse
from django.utils.text import slugify

from base.test_utils import get_richtext_json
from blog.models import BlogIndexPage, BlogPage
from blog.tests.utils import BlogIndexFormAndViewTests


class TestBlogIndexPageForm(BlogIndexFormAndViewTests):

    def test_form_basic_populates_promote_tab_fields(self):
        # Save and retrieve the newly created page
        page = self.create_index_page()
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.index_post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.index_post_data['title_fr'])
        self.assertEqual(page.search_description_en, 'Hello world!')
        self.assertEqual(page.search_description_fr, 'Salut le monde !')

    def test_form_empty_populates_promote_tab_fields(self):
        del self.index_post_data['intro_en']
        del self.index_post_data['intro_fr']
        # Save and retrieve the newly created page
        page = self.create_index_page()
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.index_post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.index_post_data['title_fr'])
        self.assertEqual(page.search_description_en, '')
        self.assertEqual(page.search_description_fr, '')

    def test_form_edit_populates_promote_tab_fields(self):
        # Save and retrieve the newly created page
        page = self.create_index_page()
        # Modify few fields and publish
        self.index_post_data['intro_en'] = get_richtext_json('Plop')
        self.index_post_data['title_fr'] = 'Liste des articles de blog'
        self.index_post_data['slug_fr'] = slugify(self.index_post_data['title_fr'])
        self.index_post_data['action-publish'] = "Publish"
        # Save new page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            reverse('wagtailadmin_pages:edit', args=(page.id,)),
            self.index_post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        # Retrieve saved page
        page = BlogIndexPage.objects.get(slug_en='blog')
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.index_post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.index_post_data['title_fr'])
        self.assertEqual(page.search_description_en, 'Plop')
        self.assertEqual(page.search_description_fr, 'Salut le monde !')


class TestBlogPageForm(BlogIndexFormAndViewTests):

    def setUp(self):
        super(TestBlogPageForm, self).setUp()
        self.index_page = self.create_index_page()

    def test_form_basic_populates_promote_tab_fields(self):
        # Save and retrieve the newly created page
        page = self.create_page()
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertEqual(page.search_description_en, self.post_data['intro_en'])
        self.assertEqual(page.search_description_fr, self.post_data['intro_fr'])

    def test_form_empty_populates_promote_tab_fields(self):
        del self.post_data['intro_fr']
        # Save and retrieve the newly created page
        page = self.create_page()
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertEqual(page.search_description_en, self.post_data['intro_en'])
        self.assertIsNone(page.search_description_fr)

    def test_form_edit_populates_promote_tab_fields(self):
        # Save and retrieve the newly created page
        page = self.create_page()
        # Modify few fields and publish
        self.post_data['intro_en'] = 'Plop'
        self.post_data['title_fr'] = 'Article de blog voilà quoi'
        self.post_data['slug_fr'] = slugify(self.post_data['title_fr'])
        self.post_data['action-publish'] = "Publish"
        # Save new page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            reverse('wagtailadmin_pages:edit', args=(page.id,)),
            self.post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        # Retrieve saved page
        page = BlogPage.objects.get(slug_en='blog-post-1')
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertEqual(page.search_description_en,  self.post_data['intro_en'])
        self.assertEqual(page.search_description_fr,  self.post_data['intro_fr'])
