from django.core.urlresolvers import reverse
from django.test.testcases import TestCase
from django.utils.text import slugify
from taggit.models import Tag
from wagtail.tests.utils import WagtailPageTests

from base.test_utils import get_richtext_json
from blog.models import BlogIndexPage, BlogPage, BlogPageTag
from home.models import HomePage


class BlogPageTests(TestCase):
    fixtures = ['site']

    def create_tag(self, title):
        tag = Tag(name=title, slug=slugify(title))
        tag.save()
        return tag

    def create_blog_tag(self, blog_page, tag):
        blog_tag = BlogPageTag(content_object=blog_page, tag=tag)
        blog_tag.save()
        return blog_tag


class BlogIndexFormAndViewTests(WagtailPageTests):
    fixtures = ['site']

    def setUp(self):
        super(BlogIndexFormAndViewTests, self).setUp()
        self.home_page = HomePage.objects.get()
        self.index_post_data = {
            'title_en': 'Blog',
            'title_fr': 'Blog',
            # Slugs are populated in javascript in wagtail admin
            'slug_en': 'blog',
            'slug_fr': 'blog',
            'intro_en': get_richtext_json('Hello world!'),
            'intro_fr': get_richtext_json('Salut le monde !'),
        }
        self.post_data = {
            'title_en': 'Blog post 1',
            'title_fr': 'Article de blog 1',
            # Slugs are populated by javascript in wagtail admin
            'slug_en': 'blog-post-1',
            'slug_fr': 'article-de-blog-1',
            'date': '2018-01-02',
            'intro_en': 'This is a wonderful intro',
            'intro_fr': "C'est une super intro",
            'body_en': get_richtext_json('Body of my first blog post'),
            'body_fr': get_richtext_json('Corps de mon premier article de blog'),
            'gallery_images-INITIAL_FORMS': 0,
            'gallery_images-MAX_NUM_FORMS': 1000,
            'gallery_images-MIN_NUM_FORMS': 0,
            'gallery_images-TOTAL_FORMS': 0,
        }

    def create_page(self, index_page=None):
        if index_page is None:
            index_page = self.index_page
        # Save new blog index page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            # args: app_name, model_name, parent_page_id
            reverse('wagtailadmin_pages:add', args=('blog', 'blogpage', index_page.id)),
            self.post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        return BlogPage.objects.get(slug_en=self.post_data['slug_en'])

    def create_index_page(self):
        # Save new blog index page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            # args: app_name, model_name, parent_page_id
            reverse('wagtailadmin_pages:add', args=('blog', 'blogindexpage', self.home_page.id)),
            self.index_post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        return BlogIndexPage.objects.get(slug_en='blog')
