from django.core.exceptions import ValidationError
from django.test.testcases import TestCase
from django.utils.text import slugify
from wagtail.images.models import Image
from wagtail.tests.utils import WagtailPageTests

from blog.models import BlogIndexPage, BlogPage, BlogPageGalleryImage, BlogPageTag
from blog.tests.utils import BlogPageTests
from home.models import HomePage


class TestBlogIndexChildAndParentTypes(WagtailPageTests):
    def test_parent_page_type(self):
        self.assertAllowedParentPageTypes(BlogIndexPage, {HomePage})

    def test_child_page_type(self):
        self.assertAllowedSubpageTypes(BlogIndexPage, {BlogPage})


class TestBlogPageChildAndParentTypes(WagtailPageTests):
    def test_parent_page_type(self):
        self.assertAllowedParentPageTypes(BlogPage, {BlogIndexPage})

    def test_child_page_type(self):
        self.assertAllowedSubpageTypes(BlogPage, [])


class TestBlogIndexPageModelFields(TestCase):
    fixtures = ['site']

    def setUp(self):
        super(TestBlogIndexPageModelFields, self).setUp()
        self.home_page = HomePage.objects.get()

    def test_minimal_page(self):
        blog_index_page = BlogIndexPage(
            title_en='Blog',
            title_fr='Blog',
            slug_en=slugify('Blog'),
            slug_fr=slugify('Blog'),
        )
        self.home_page.add_child(instance=blog_index_page)
        # check that about_us_page exists in the db
        retrieved_page = BlogIndexPage.objects.get(id=blog_index_page.id)
        # assert all fields
        self.assertEqual(retrieved_page.title_en, blog_index_page.title_en)
        self.assertEqual(retrieved_page.title_fr, blog_index_page.title_fr)
        self.assertEqual(retrieved_page.slug_en, blog_index_page.slug_en)
        self.assertEqual(retrieved_page.slug_fr, blog_index_page.slug_fr)
        self.assertIsNone(retrieved_page.intro_en)
        self.assertIsNone(retrieved_page.intro_fr)

    def test_full_page(self):
        blog_index_page = BlogIndexPage(
            title_en='Blog',
            title_fr='Blog',
            slug_en=slugify('Blog'),
            slug_fr=slugify('Blog'),
            intro_en='<p>Hello guys!</p>',
            intro_fr='<p>Salut les gars !</p>',
        )
        self.home_page.add_child(instance=blog_index_page)
        # check that about_us_page exists in the db
        retrieved_page = BlogIndexPage.objects.get(id=blog_index_page.id)
        # assert all fields
        self.assertEqual(retrieved_page.title_en, blog_index_page.title_en)
        self.assertEqual(retrieved_page.title_fr, blog_index_page.title_fr)
        self.assertEqual(retrieved_page.slug_en, blog_index_page.slug_en)
        self.assertEqual(retrieved_page.slug_fr, blog_index_page.slug_fr)
        self.assertEqual(retrieved_page.intro_en, blog_index_page.intro_en)
        self.assertEqual(retrieved_page.intro_fr, blog_index_page.intro_fr)


class TestBlogPageModel(BlogPageTests):

    def setUp(self):
        super(TestBlogPageModel, self).setUp()
        self.home_page = HomePage.objects.get()
        self.blog_index_page = BlogIndexPage(
            title_en='Blog',
            title_fr='Blog',
            slug_en=slugify('Blog'),
            slug_fr=slugify('Blog'),
            intro_en='<p>Hello guys!</p>',
            intro_fr='<p>Salut les gars !</p>',
        )
        self.home_page.add_child(instance=self.blog_index_page)

    def test_minimal_page(self):
        blog_page = BlogPage(
            title_en='My first blog post',
            title_fr='Mon premier article de blog',
            slug_en=slugify('My first blog post'),
            slug_fr=slugify('Mon premier article de blog'),
            date='2018-01-02',
            intro_en='This is a wonderful intro',
        )
        self.blog_index_page.add_child(instance=blog_page)
        # check that about_us_page exists in the db
        retrieved_page = BlogPage.objects.get(id=blog_page.id)
        # assert all fields
        self.assertEqual(retrieved_page.title_en, blog_page.title_en)
        self.assertEqual(retrieved_page.title_fr, blog_page.title_fr)
        self.assertEqual(retrieved_page.slug_en, blog_page.slug_en)
        self.assertEqual(retrieved_page.slug_fr, blog_page.slug_fr)
        self.assertEqual(retrieved_page.date, blog_page.date)
        self.assertEqual(retrieved_page.intro_en, blog_page.intro_en)
        self.assertIsNone(retrieved_page.intro_fr)
        self.assertIsNone(retrieved_page.body_en)
        self.assertIsNone(retrieved_page.body_fr)
        self.assertEqual(retrieved_page.tags.count(), 0)
        self.assertIsNone(retrieved_page.main_image())

    def test_missing_fields_page(self):
        with self.assertRaises(ValidationError):
            self.blog_index_page.add_child(instance=BlogPage(
                title_en='My first blog post',
                title_fr='Mon premier article de blog',
                slug_en=slugify('My first blog post'),
                slug_fr=slugify('Mon premier article de blog'),
                intro_en='This is a wonderful intro',
                intro_fr="C'est une super intro",
            ))
        with self.assertRaises(ValidationError):
            self.blog_index_page.add_child(instance=BlogPage(
                title_en='My first blog post',
                title_fr='Mon premier article de blog',
                slug_en=slugify('My first blog post'),
                slug_fr=slugify('Mon premier article de blog'),
                date='2018-01-02',
                intro_fr="C'est une super intro",
            ))

    def test_full_page(self):
        blog_page = BlogPage(
            title_en='My first blog post',
            title_fr='Mon premier article de blog',
            slug_en=slugify('My first blog post'),
            slug_fr=slugify('Mon premier article de blog'),
            date='2018-01-02',
            intro_en='This is a wonderful intro',
            intro_fr="C'est une super intro",
            body_en='<p>Body of my first blog post</p>',
            body_fr='<p>Corps de mon premier article de blog</p>',
        )
        self.blog_index_page.add_child(instance=blog_page)

        # Wordcloud image
        image_1 = BlogPageGalleryImage(
            page=blog_page,
            image=Image.objects.get(),
            caption_en="foobar",
            caption_fr="toto",
            sort_order=1,
        )
        image_1.save()

        # Image 2
        image2 = Image(title='image2', file='../media/original_images/wordcloud3.png')
        image2.save()
        image_2 = BlogPageGalleryImage(
            page=blog_page,
            image=image2,
            sort_order=2,
        )
        image_2.save()
        # Check that the second image exists in DB
        retrieved_image_2 = BlogPageGalleryImage.objects.get(id=image_2.id)
        self.assertEqual(retrieved_image_2.image.id, image2.id)
        self.assertEqual(retrieved_image_2.page.id, blog_page.id)
        self.assertIsNone(retrieved_image_2.caption_en)
        self.assertIsNone(retrieved_image_2.caption_fr)

        # Tags
        # First tag
        tag_1 = self.create_tag('Foo')
        blog_tag_1 = self.create_blog_tag(blog_page, tag_1)
        retrieved_blog_tag_1 = BlogPageTag.objects.get(id=blog_tag_1.id)
        self.assertEqual(retrieved_blog_tag_1.tag, tag_1)
        self.assertEqual(retrieved_blog_tag_1.content_object, blog_page)
        # Second tag
        tag_2 = self.create_tag('Bar')
        blog_tag_2 = self.create_blog_tag(blog_page, tag_2)
        retrieved_blog_tag_2 = BlogPageTag.objects.get(id=blog_tag_2.id)
        self.assertEqual(retrieved_blog_tag_2.tag, tag_2)
        self.assertEqual(retrieved_blog_tag_2.content_object, blog_page)

        # check that blog page exists in the db
        retrieved_page = BlogPage.objects.get(id=blog_page.id)
        # assert all fields
        self.assertEqual(retrieved_page.title_en, blog_page.title_en)
        self.assertEqual(retrieved_page.title_fr, blog_page.title_fr)
        self.assertEqual(retrieved_page.slug_en, blog_page.slug_en)
        self.assertEqual(retrieved_page.slug_fr, blog_page.slug_fr)
        self.assertEqual(retrieved_page.date, blog_page.date)
        self.assertEqual(retrieved_page.intro_en, blog_page.intro_en)
        self.assertEqual(retrieved_page.intro_fr, blog_page.intro_fr)
        self.assertEqual(retrieved_page.body_en, blog_page.body_en)
        self.assertEqual(retrieved_page.body_fr, blog_page.body_fr)
        self.assertEqual(retrieved_page.tags.count(), 2)
        self.assertEqual(retrieved_page.tags.first().id, blog_tag_1.id)
        self.assertEqual(retrieved_page.tags.last().id, blog_tag_2.id)
        self.assertEqual(retrieved_page.main_image(), image_1.image)
        self.assertEqual(retrieved_page.gallery_images.count(), 2)
