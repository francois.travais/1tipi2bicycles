from django.conf.urls import url

from blog import views

urlpatterns = [
    url(r'^tag/(?P<tag>[-\w]+)/', views.tag_view, name="tag"),
]
