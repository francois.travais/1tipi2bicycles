from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel

from base.models import OttbPageWithRichIntroMixin


class AboutUsPage(OttbPageWithRichIntroMixin, Page):
    intro = RichTextField(blank=True)
    floriane_picture = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.SET_NULL, related_name='+', null=True,
        verbose_name='Floriane\'s picture'
    )
    francois_picture = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.SET_NULL, related_name='+', null=True,
        verbose_name='François\' picture'
    )
    floriane_description = RichTextField(blank=True)
    francois_description = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        MultiFieldPanel([
            ImageChooserPanel('floriane_picture'),
            FieldPanel('floriane_description'),
        ], heading="Floriane"),
        MultiFieldPanel([
            ImageChooserPanel('francois_picture'),
            FieldPanel('francois_description'),
        ], heading="François"),
    ]

    # About us page is a direct child of the home page
    parent_page_types = ['home.HomePage']

    # This page shouldn't have a child page
    subpage_types = []
