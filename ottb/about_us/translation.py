from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from about_us.models import AboutUsPage


@register(AboutUsPage)
class AboutUsPageTR(TranslationOptions):
    fields = (
        'intro',
        'floriane_description',
        'francois_description',
    )
