from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.test.testcases import TestCase
from django.utils.text import slugify
from wagtail.images.models import Image
from wagtail.tests.utils import WagtailPageTests

from about_us.models import AboutUsPage
from base.test_utils import get_richtext_json
from home.models import HomePage


class TestAboutUsPageChildAndParentTypes(WagtailPageTests):
    def test_parent_page_type(self):
        self.assertAllowedParentPageTypes(AboutUsPage, {HomePage})

    def test_child_page_type(self):
        self.assertAllowedSubpageTypes(AboutUsPage, [])


class TestAboutUsPageModel(TestCase):
    fixtures = ['site']

    def setUp(self):
        super(TestAboutUsPageModel, self).setUp()
        self.home_page = HomePage.objects.get()

    def test_minimal_page(self):
        about_us_page = AboutUsPage(
            title_en='About Us',
            title_fr='Qui nous sommes',
            slug_en=slugify('About us'),
            slug_fr=slugify('Qui nous sommes'),
            floriane_picture=Image.objects.get(),
            francois_picture=Image.objects.get(),
        )
        self.home_page.add_child(instance=about_us_page)
        # check that about_us_page exists in the db
        retrieved_page = AboutUsPage.objects.get(id=about_us_page.id)
        # assert all fields
        self.assertEqual(retrieved_page.title_en, about_us_page.title_en)
        self.assertEqual(retrieved_page.title_fr, about_us_page.title_fr)
        self.assertEqual(retrieved_page.slug_en, about_us_page.slug_en)
        self.assertEqual(retrieved_page.slug_fr, about_us_page.slug_fr)
        self.assertEqual(retrieved_page.floriane_picture, about_us_page.floriane_picture)
        self.assertEqual(retrieved_page.francois_picture, about_us_page.francois_picture)
        self.assertIsNone(retrieved_page.intro_en)
        self.assertIsNone(retrieved_page.intro_fr)
        self.assertIsNone(retrieved_page.francois_description_en)
        self.assertIsNone(retrieved_page.francois_description_fr)
        self.assertIsNone(retrieved_page.floriane_description_en)
        self.assertIsNone(retrieved_page.floriane_description_fr)

    def test_pictures_are_required(self):
        with self.assertRaises(ValidationError):
            self.home_page.add_child(instance=AboutUsPage(
                title_en='About Us',
                title_fr='Qui nous sommes',
                slug_en=slugify('About us'),
                slug_fr=slugify('Qui nous sommes'),
                francois_picture=Image.objects.get(),
            ))
        with self.assertRaises(ValidationError):
            self.home_page.add_child(instance=AboutUsPage(
                title_en='About Us',
                title_fr='Qui nous sommes',
                slug_en=slugify('About us'),
                slug_fr=slugify('Qui nous sommes'),
                floriane_picture=Image.objects.get(),
            ))

    def test_full_page(self):
        about_us_page = AboutUsPage(
            title_en='About Us',
            title_fr='Qui nous sommes',
            slug_en=slugify('About us'),
            slug_fr=slugify('Qui nous sommes'),
            intro_en='<p>Hello world!</p>',
            intro_fr='<p>Salut le monde !</p>',
            floriane_picture=Image.objects.get(),
            floriane_description_en='<p>Hello my name is Floriane</p>',
            floriane_description_fr="<p>Salut je m'appelle Floriane</p>",
            francois_picture=Image.objects.get(),
            francois_description_en='<p>Hello my name is François</p>',
            francois_description_fr="<p>Salut je m'appelle François</p>",
        )
        self.home_page.add_child(instance=about_us_page)
        # check that about_us_page exists in the db
        retrieved_page = AboutUsPage.objects.get(id=about_us_page.id)
        # assert all fields
        self.assertEqual(retrieved_page.title_en, about_us_page.title_en)
        self.assertEqual(retrieved_page.title_fr, about_us_page.title_fr)
        self.assertEqual(retrieved_page.slug_en, about_us_page.slug_en)
        self.assertEqual(retrieved_page.slug_fr, about_us_page.slug_fr)
        self.assertEqual(retrieved_page.intro_en, about_us_page.intro_en)
        self.assertEqual(retrieved_page.intro_fr, about_us_page.intro_fr)
        self.assertEqual(retrieved_page.floriane_picture, about_us_page.floriane_picture)
        self.assertEqual(retrieved_page.floriane_description_en, about_us_page.floriane_description_en)
        self.assertEqual(retrieved_page.floriane_description_fr, about_us_page.floriane_description_fr)
        self.assertEqual(retrieved_page.francois_picture, about_us_page.francois_picture)
        self.assertEqual(retrieved_page.francois_description_en, about_us_page.francois_description_en)
        self.assertEqual(retrieved_page.francois_description_fr, about_us_page.francois_description_fr)


class FormAndViewTests(WagtailPageTests):
    fixtures = ['site']

    def setUp(self):
        super(FormAndViewTests, self).setUp()
        self.home_page = HomePage.objects.get()
        self.post_data = {
            'title_en': 'About us',
            'title_fr': 'Qui nous sommes',
            # Slugs are populated in javascript in wagtail admin
            'slug_en': 'about-us',
            'slug_fr': 'qui-nous-sommes',
            'intro_en': get_richtext_json('Hello world!'),
            'intro_fr': get_richtext_json('Salut le monde !'),
            'floriane_picture': '1',
            'floriane_description_en': get_richtext_json('Hello my name is Florian'),
            'floriane_description_fr': get_richtext_json("Salut je m'appelle Floriane"),
            'francois_picture': '1',
            'francois_description_en': get_richtext_json('Hello my name is François'),
            'francois_description_fr': get_richtext_json("Salut je m'appelle François"),
        }

    def create_page(self):
        # Save new page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            # args: app_name, model_name, parent_page_id
            reverse('wagtailadmin_pages:add', args=('about_us', 'aboutuspage', self.home_page.id)),
            self.post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        # Retrieve saved page
        return AboutUsPage.objects.get(slug_en=self.post_data['slug_en'])


class TestAboutUsPageForm(FormAndViewTests):

    def test_form_basic_populates_promote_tab_fields(self):
        # Save and retrieve the newly created page
        page = self.create_page()
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertEqual(page.search_description_en, 'Hello world!')
        self.assertEqual(page.search_description_fr, 'Salut le monde !')

    def test_form_empty_populates_promote_tab_fields(self):
        del self.post_data['intro_en']
        del self.post_data['intro_fr']
        # Save and retrieve the newly created page
        page = self.create_page()
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertEqual(page.search_description_en, '')
        self.assertEqual(page.search_description_fr, '')

    def test_form_edit_populates_promote_tab_fields(self):
        # Save and retrieve the newly created page
        page = self.create_page()
        # Modify few fields and publish
        self.post_data['intro_en'] = get_richtext_json('Plop')
        self.post_data['title_fr'] = 'Nous quoi'
        self.post_data['slug_fr'] = slugify(self.post_data['title_fr'])
        self.post_data['action-publish'] = "Publish"
        # Save new page using wagtailadmin in order to get all the form mechanisms working
        response = self.client.post(
            reverse('wagtailadmin_pages:edit', args=(page.id,)),
            self.post_data
        )
        # Should redirect to edit page (normal wagtail behaviour)
        self.assertEqual(response.status_code, 302)
        # Retrieve saved page
        page = AboutUsPage.objects.get(slug_en='about-us')
        # Assert that our custom form fill the promote tab fields
        self.assertEqual(page.seo_title_en, self.post_data['title_en'])
        self.assertEqual(page.seo_title_fr, self.post_data['title_fr'])
        self.assertEqual(page.search_description_en, 'Plop')
        self.assertEqual(page.search_description_fr, 'Salut le monde !')


class TestAboutUsPageView(FormAndViewTests):

    def test_view_get_english(self):
        # Publish after creation
        self.post_data['action-publish'] = "Publish"
        # Save and retrieve the newly created page
        page = self.create_page()
        response = self.client.get('/en/about-us/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('about_us/about_us_page.html')
        # Title
        self.assertTagInHTML('<title>%s - localhost</title>' % str(page.title_en), result, count=1)
        self.assertTagInHTML('<h1>%s</h1>' % str(page.title_en), result, count=1, allow_extra_attrs=True)
        self.assertNotInHTML('<title>%s - localhost</title>' % str(page.title_fr), result)
        self.assertNotInHTML(page.title_fr, result)
        # Other fields
        self.assertIn(page.intro_en, result)
        self.assertNotIn(page.intro_fr, result)
        self.assertIn(page.floriane_description_en, result)
        self.assertNotIn("<p>Salut je m'appelle Floriane</p>", result)
        self.assertIn('<p>Hello my name is François</p>', result)
        self.assertNotIn("<p>Salut je m'appelle François</p>", result)
        self.assertTagInHTML('<img alt="wordcloud3.png" width="200" height="200" />', result, count=2,
                             allow_extra_attrs=True)

    def test_view_get_french(self):
        # Publish after creation
        self.post_data['action-publish'] = "Publish"
        # Save and retrieve the newly created page
        page = self.create_page()
        response = self.client.get('/fr/qui-nous-sommes/')
        result = response.content.decode(response.charset)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('about_us/about_us_page.html')
        # Title
        self.assertTagInHTML('<title>%s - localhost</title>' % str(page.title_fr), result, count=1)
        self.assertTagInHTML('<h1>%s</h1>' % str(page.title_fr), result, count=1, allow_extra_attrs=True)
        self.assertNotInHTML('<title>%s - localhost</title>' % str(page.title_en), result)
        self.assertNotInHTML(page.title_en, result)
        # Other fields
        self.assertIn(page.intro_fr, result)
        self.assertNotIn(page.intro_en, result)
        self.assertIn("<p>Salut je m&#x27;appelle Floriane</p>", result)
        self.assertNotIn(page.floriane_description_en, result)
        self.assertIn("<p>Salut je m&#x27;appelle François</p>", result)
        self.assertNotIn('<p>Hello my name is François</p>', result)
        self.assertTagInHTML('<img alt="wordcloud3.png" width="200" height="200" />', result, count=2,
                             allow_extra_attrs=True)
