from django import template

register = template.Library()


@register.inclusion_tag('cookie_consent/tags/cookie_consent.html')
def cookie_consent():
    return {}
