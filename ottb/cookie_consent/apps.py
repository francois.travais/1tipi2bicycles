from django.apps import AppConfig


class CookieConsentConfig(AppConfig):
    name = 'cookie_consent'
