from django.db import models
from wagtail.contrib.settings.models import BaseSetting, register_setting


@register_setting
class ContactSettings(BaseSetting):
    """Contact details displayed in the footer."""
    email = models.EmailField(default='francois.travais@gmail.com',
                              help_text='Email link in the footer of each page')


@register_setting
class MailChimpSettings(BaseSetting):
    """Settings configurable in the Wagtail admin related to MailChimp.

    This makes it possible to modify the signup urls from within the Wagtail
    editor under the Settings menu item.
    """
    signup_url_en = models.URLField(
        default='http://eepurl.com/c3LFCj',
        help_text='MailChimp sign up form for English list')
    signup_url_fr = models.URLField(
        default='http://eepurl.com/c3LHWb',
        help_text='MailChimp sign up form for French list')


@register_setting
class SocialMediaSettings(BaseSetting):
    """Settings configurable in the Wagtail admin related to social media.

    This makes it possible to modify the social media urls from within the Wagtail
    editor under the Settings menu item.
    """
    facebook_url = models.URLField(
        help_text='Facebook page URL',
        blank=True)
    instagram_url = models.URLField(
        help_text='Instagram account URL',
        blank=True)
    twitter_url = models.URLField(
        help_text='Twitter account URL',
        blank=True)
    gitlab_url = models.URLField(
        help_text='Gitlab repository URL',
        blank=True)
