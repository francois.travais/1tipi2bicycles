from datetime import datetime, time

from django.contrib.syndication.views import Feed
from django.utils.translation import ugettext_lazy as _

from blog.models import BlogPage
from roadbook.models import RoadbookLegPage


class BlogLatestUpdates(Feed):
    title = _('1 Tipi 2 Bicycles')
    description = '%s %s' % (_('1 Tipi 2 Bicycles'), _('blog posts.'))
    link = '/'

    def items(self):
        """Return an ordered list of items to be put into the RSS feed."""
        return BlogPage.objects.live().order_by('-date')[:5]

    def item_title(self, item: BlogPage):
        return item.title

    def item_description(self, item: BlogPage):
        return item.intro

    def item_link(self, item: BlogPage):
        return item.full_url  # Return an absolute URL

    def item_pubdate(self, item: BlogPage):
        return datetime.combine(item.date, time(hour=12))

    def item_categories(self, item: BlogPage):
        return [str(tag) for tag in item.tags.all()]


class RoadbookLatestUpdates(Feed):
    title = _('1 Tipi 2 Bicycles')
    description = '%s %s' % (_('1 Tipi 2 Bicycles'), _('road book legs.'))
    link = '/'

    def items(self):
        """Return an ordered list of items to be put into the RSS feed."""
        return RoadbookLegPage.objects.live().order_by('-start_date')[:5]

    def item_title(self, item: RoadbookLegPage):
        return item.title

    def item_description(self, item: RoadbookLegPage):
        return item.description

    def item_link(self, item: RoadbookLegPage):
        return item.full_url  # Return an absolute URL

    def item_pubdate(self, item: RoadbookLegPage):
        return datetime.combine(item.first_published_at, time(hour=12))
