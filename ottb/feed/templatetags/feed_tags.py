from django import template
from django.utils.translation import get_language_from_request

from feed.models import MailChimpSettings

register = template.Library()


@register.simple_tag(takes_context=True)
def signup_url(context):
    """Return the appropriate MailChimp signup URL for the current language."""
    language = get_language_from_request(context.request, check_path=True)
    settings = MailChimpSettings.for_site(context.request.site)

    if language == 'fr':
        return settings.signup_url_fr
    return settings.signup_url_en
