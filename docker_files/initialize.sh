#!/usr/bin/env sh

set -ex

export DOCKER_HOST_IP=$(ip route show | awk '/default/ {print $3}')
export REDIS_HOST=${REDIS_HOST:-$DOCKER_HOST_IP}
export POSTGRES_HOST=${POSTGRES_HOST:-$DOCKER_HOST_IP}

# Validate nginx configuration
/usr/sbin/nginx -t -c /1tipi2bicycl.es/docker_files/ottb.nginx

# Copy django config to the right place
if [ -f "$CONF_PATH/local.py" ]; then
    cp -f "$CONF_PATH/local.py" "$INSTALL_PATH/ottb/ottb/settings/local.py"
fi

# Migrate DB
python3 ottb/manage.py migrate --no-input

# Sync translated fields
python3 ottb/manage.py sync_page_translation_fields --noinput

# Start nginx and uwsgi
/usr/bin/supervisord -c docker_files/supervisord.conf
