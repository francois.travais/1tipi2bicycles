# Python version currently in production
FROM python:3.6.7-alpine3.8

LABEL maintainer="François Travais <francois.travais@gmail.com>" license=Apache-2.0

# Our workspace, logs and conf dirs
ENV INSTALL_PATH=/1tipi2bicycl.es \
    LOG_PATH=/var/log/ottb \
    CONF_PATH=/etc/ottb \
    CERTS_PATH=/etc/ssl/certs/ottb
RUN mkdir -p $INSTALL_PATH $LOG_PATH $CONF_PATH $CERTS_PATH
WORKDIR $INSTALL_PATH
VOLUME $LOG_PATH
VOLUME $CONF_PATH
VOLUME $CERTS_PATH

COPY ottb/ottb/requirements ottb/ottb/requirements/

# Build and python deps + remove useless files to make the image thinner
# See https://blog.codeship.com/alpine-based-docker-images-make-difference-real-world-apps/
RUN set -ex \
	&& apk add --no-cache --virtual .build-deps \
		g++ \
		gcc \
		git \
		libc-dev \
		libjpeg-turbo-dev \
		libxml2-dev \
		libxslt-dev \
		linux-headers \
		make \
		musl-dev \
		pcre-dev \
		postgresql-dev \
	&& pip install -U pip \
	&& pip install -r ottb/ottb/requirements/production.txt \
	&& find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + \
	&& runDeps="$( \
        scanelf --needed --nobanner --recursive /usr/local \
                | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                | sort -u \
                | xargs -r apk info --installed \
                | sort -u \
    )" \
    && apk add --virtual .rundeps $runDeps \
    && apk del .build-deps

# Install other dependencies
RUN set -ex \
    && apk add --no-cache \
        gettext \
        libmagic \
        nginx \
        nodejs-npm \
        postgresql-client \
        supervisor \
        uwsgi \
        uwsgi-python3 \
    && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/main \
        libcrypto1.1 \
    && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing \
        gdal

# Frontend dependencies
COPY package.json package-lock.json ./

# Install frontend dependencies
RUN npm install

# Add custom environment variables needed by Django or your settings file here:
ARG DJANGO_SETTINGS_MODULE=ottb.settings.production
ARG DJANGO_SECRET_KEY
ENV DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE \
    DJANGO_SECRET_KEY=$DJANGO_SECRET_KEY \
    DJANGO_STATIC_FILES_PATH=/var/www/1tipi2bicycles

COPY ottb/ ottb/

# Collect static files
RUN cd ottb/ \
    && mkdir -p $DJANGO_STATIC_FILES_PATH ottb/static \
    && python3 manage.py collectstatic --clear --no-input

# Compress front end code
RUN cd ottb/ \
    && python3 manage.py compress

# Compile i18n messages
RUN cd ottb/ \
    && python3 manage.py compilemessages

# Configure Nginx and uwsgi
COPY docker_files/ docker_files/
RUN cp -f docker_files/ottb.nginx /etc/nginx/conf.d/ottb.conf \
    && install -o uwsgi -d /run/uwsgi \
    && install -o nginx -d /run/nginx

EXPOSE 443

# start supervisord
CMD ["/bin/sh", "docker_files/initialize.sh"]
